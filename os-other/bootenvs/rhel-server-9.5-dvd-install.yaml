---
Name: rhel-server-9.5-dvd-install
Description: Install BootEnv for RedHat 9 (RHEL) - full DVD ISO
Documentation: |
  This BootEnv installs the RHEL Server 9 operating system from the full dvd
  ISO. By default, it will install as a trial with no registration. This is
  specified by the `redhat/subscription-username` parameter with the default
  username of "trial" and `redhat/subscription-password` blank. Adding a
  password and/or changing the username will be verified by RedHat's servers
  and the install will hang if it fails. You can also completely skip
  registration even with the username and password parameters set by adding
  the `redhat/subscription-skip-activation` parameter.

  The ISO can be downloaded from the RedHat Access website with an authorized
  login and account.  The website is typically found at:

    * <https://access.redhat.com/downloads/content/479/ver=/rhel---9/9.5/x86_64/product-software>
    * <https://access.redhat.com/downloads/content/419/ver=/rhel---9/9.5/aarch64/product-software>

Meta:
  color: red
  feature-flags: change-stage-v2
  group-by: RedHat
  icon: linux
  title: Digital Rebar Community Content
Loaders:
  amd64-uefi: EFI/BOOT/BOOTX64.EFI
  arm64-uefi: EFI/BOOT/BOOTAA64.EFI
OS:
  Name: rhel-server-9.5-dvd
  Codename: ""
  Family: redhat
  Version: "9.5"
  SupportedArchitectures:
    x86_64:
      IsoFile: "rhel-9.5-x86_64-dvd.iso"
      Sha256: "0bb7600c3187e89cebecfcfc73947eb48b539252ece8aab3fe04d010e8644ea9"
      IsoUrl: ""
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    aarch64:
      IsoFile: "rhel-9.5-aarch64-dvd.iso"
      Sha256: "a71f5fc2e1587e9732b6a6f441c9a4a2570a0aa18faeb81f77d4981082dc0f50"
      IsoUrl: ""
      Kernel: images/pxeboot/vmlinuz
      Initrds:
        - images/pxeboot/initrd.img
      BootParams: >-
        inst.ks.device=bootif
        inst.ks={{.Machine.Url}}/compute.ks
        inst.repo={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
OnlyUnknown: false
OptionalParams:
  - kernel-console
  - kernel-options
  - ntp-servers
  - operating-system-disk
  - proxy-servers
  - provisioner-default-password-hash
  - provisioner-default-username
  - provisioner-access-key
  - provisioner-network-config
  - select-kickseed
  - kickstart-base-packages
  - extra-packages
  - redhat/kickstart-shell
  - redhat/rhsm-activation-key
  - redhat/rhsm-additional
  - redhat/rhsm-organization
  - redhat/subscription-gpg-keys
  - redhat/subscription-username
  - redhat/subscription-password
  - redhat/subscription-repos
  - redhat/subscription-skip-activation
Templates:
  - Name: kexec
    ID: kexec.tmpl
    Path: "{{.Machine.Path}}/kexec"
  - Name: pxelinux
    ID: default-pxelinux.tmpl
    Path: pxelinux.cfg/{{.Machine.HexAddress}}
  - Name: ipxe
    ID: default-ipxe.tmpl
    Path: "{{.Machine.Address}}.ipxe"
  - Name: pxelinux-mac
    ID: default-pxelinux.tmpl
    Path: pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}
  - Name: ipxe-mac
    ID: default-ipxe.tmpl
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
  - Name: grub
    ID: default-grub.tmpl
    Path: grub/{{.Machine.Address}}.cfg
  - Name: grub-mac
    ID: default-grub.tmpl
    Path: grub/{{.Machine.MacAddr "grub"}}.cfg
  - ID: default-grub.tmpl
    Name: grub-secure
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
  - Name: compute.ks
    ID: select-kickseed.tmpl
    Path: "{{.Machine.Path}}/compute.ks"
