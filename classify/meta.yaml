Author: RackN
CodeSource: https://gitlab.com/rackn/provision-content
Color: black
Copyright: RackN 2019
Description: Defines custom actions on Machines if they match tests.
DisplayName: Classify
DocUrl: https://docs.rackn.io/stable/redirect/?ref=rs_content_classify
Documentation: |-
  The Classifier System allows an operator to flexibly define groups of
  *test*/*action* sequences to perform on a Machine.  These can be used
  to do any number of tasks.  Typically the classifier allows an operator
  to add additional information to a Machine object that will alter the
  behavior of subsequent Workflow stages/tasks.

  By default the classifier provides a limited set of *tests* and *actions*
  that can be performed.  However, the operator can specify additional
  groups of *test*/*actions* pairs by use of the custom functions feature.
  See the Param `classify/custom-functions` documentation on how to use it.

  By default, the classifier is designed to exit on the first test match
  that succeeds with a "pass" status.  The operator can override this
  behavior by setting the optional `continue` value set to `true`
  in the `classify/classification-data` structure.

  If you are extending the Classifier to add custom groups of *test*/*action*
  sequences, please review the `function ... {}` BASH functions in the
  `classify.sh.tmpl` **template** for examples.


  ## Versions

  The Classifier can operate in two modes, defined by the `classify/version`
  parameter. The main difference between these modes is that version 2 uses
  params that specify the names of other params. Those indirectly referenced params
  provide the classification data either through configuration or default value.

  Version 2 is the preferred way of using the classifier, but `classify/version`
  defaults to 1 for historical purposes. Both are driven from the same `classify` task.

  ### Classifier 1

  Classifier version 1 is configured on a profile, usually the global profile.
  It has some limitations if you're trying to do pipelines or need to be able to
  have multiple sets of classification rules.

  It uses the following params:

    - `classify/classification-data`: list of tests and actions for each test
    - `classify/custom-functions`: list of templates with the custom functions
    - `classify/disable-classifier`: default true, disables classifier

  ### Classifier 2

  Classifier version 2 is intended to be configured on a stage. This allows for creating
  stages that do their own classification rather than being limited to one
  classifier configuration for the whole workflow.

  A version 2 classifier stage uses the following params:

    - `classify/data-parameter`: name of param that has `classification-data`
    - `classify/function-parameter`: name of param that has the list of templates
    - `classify/disable-parameter`: name of param that disables classifier

  On stages with the following configuration, stages in the `stage-list-parameter`
  param will automatically be inserted into the workflow.

    - `Tasks: [classify-stage-list-start, classify-stage-list-stop]`
    - `Params.classify/stage-list-parameter`: name of param that has a list of stages

  One of the stages in the stage list parameter is usually a version 2 classifier stage.

  !!! note
      The parameters used by the version 2 classifier usually have default values
      specifically so users can inject their own modifications via params or profiles
      without having to make changes to the original content pack.


  ## Getting Started with the Classifier

  The Classifier rulesets of *test*/*action* sequences are configured by setting the Param
  `classify/classification-data` (v1) or being the target of a `classify/data-parameter` (v2) on a Machine.  The usual rules and orders of precedence
  are followed on this Param (Param on Machine; Profile on Machine; Global Profile; etc...).

  The classification data structure is a list of YAML or JSON objects, with two required and one
  optional set of values.  The structure is as follows (example in YAML):

    ```yaml
      - test: "<TEST_TYPE>"
        actions:
          - "<ACTION>"
          - "<ACTION>"
        continue: true
    ```

  The `test` and `actions` are required elements.  `continue` is optional based on the
  operators need. `continue` is `true` by default.

  Here are some example usage scenarious to help you get started.

  ## Simple MAC Address Match and Classify

  In this scenario, you will use the MAC Address as a simple *test* matcher
  to apply *actions*.  Demonstrated in this example are adding two Params, and two profiles
  to the machine.

  The Params in this example sets hostname, and `universal/application` value to specify the
  *role* that this machine will perform.  Subsequent content/plugins can react accordingly to
  this.  The profiles might be responsible for carrying configuration data for subsequent
  content/plugins to operate on.  For example, setting BIOS `bios-target-configuraion`,
  determining what Firmware/Flash versions to use, Operating System configuration profile
  data (eg VMware ESXi configuration, etc.).

  Last, this classify test/action sequence will continue on in the list to any subsequent
  test/action sequences after a match is found (`continue: true`).

    ```yaml
    classify/classification-data:
      - test: "has_mac 24:6e:96:6a:40:34"
        actions:
          - "set_parameter hostname r3r6-u03"
          - "set_parameter universal/application vcf381"
          - "add_profile demo-r3r6-machine-r3r6-u03"
          - "add_profile demo-r3r6-application-vcf381"
        continue: true
    ```


  ## Change Workflow

  In this scenario, the Classify system will be used to change the Machines Workflow.  We
  will match on the Inventory field `inventory/Manufacturer` with a match of `QEMU`
  (which is typically how a KVM hypervisor will present a KVM backed Virtual Machine). The
  Machine will be changed to the `centos-install` workflow to install CentOS operating
  system.

  !!! note
      When the Machine's workflow is changed, any remaining Stages/Tasks from the **current**
      workflow will be immediately replaced by the new Workflow.

  This example assumes that the operator has already set the `classify/custom-functions` to include
  the `has_inventory_value.sh.tmpl` template included already.

    ```yaml
    classify/classification-data:
      - test: "has_inventory_value Manufacturer QEMU"
        actions:
          - "change_workflow centos-install"
    ```

  Since there is no `continue: true` setting, the classifier will exit after this rule, and
  will not process any further rules listed after this one.

  !!! note
      The `Flexiflow` content pack is designed to provide controlled ways to inject new
      **tasks** and switch **workflows**.  You may wish to utilize the constructs in that
      content pack over the `change_workflow` action in the Classifier.

  ## Annotations

  The RackN Portal parses special annotations from templates referenced by the custom function params.
  These annotations must be prefixed with two hash symbols (`##`).

  |Name|Example|Description|
  |-|-|-|
  |`@type`|`@type name ^regex$`|Allows for the definition of custom types to be used in @action or @test.|
  |none|`Description`|A comment without an annotation next to the following annotations will be considered the function description.|
  |`@name`|`@name Display Name`|Changes the name displayed in the portal for the test or action.|
  |`@action`|`@action funcname(argName: type, arg2: type)`|For a classifier action, describes the shell function name, argument names and types.|
  |`@test`|`@test funcname(argName: type, arg2: type)`|For a classifier test, describes the shell function name, argument names and types.|
  |`@hint`|`@hint argName Hint text`|Provides a hint for what the named argument in a function does.|
  |`@icon`|`@icon iconname`|Sets an icon for this function in the RackN Portal.|
  |`@deprecated`|`@deprecated Message`|Displays a deprecation warning when this function is used in the RackN Portal.|

  The `@type` annotation uses JavaScript Regular Expressions. It does not have to be next to the other annotations and
  can be shared between multiple functions. If no type annotation is found for the type on a function, the type is ignored.
  If a type annotation is found in the RackN Portal and the user input value does not match the pattern, the input is
  colored red.

  ### Annotation Example

  ```sh
  ## @type yesno ^(yes|no)$

  ## The description is not in any annotation
  ## @name Name of the action displayed in the Portal
  ## @action my_action(arg1: yesno, arg2: string)
  ## @hint arg1 enter yes or no
  ## @hint arg2 custom placeholder text
  ## @icon pencil
  ## @deprecated Deprecation warning text
  function my_action() {
      echo "arg 1 $1"
      echo "arg 2 $2"
      exit 0
  }

  ## Passes if the argument is yes
  ## @name Is Yes
  ## @test cl_is_yes(arg: yesno)
  ## @hint arg yes or no
  ## @icon check
  function cl_is_yes() {
    if [[ "$1" == "yes" ]]; then
      echo "pass"
    else
      echo "fail"
    fi
  }
  ```

Icon: search
License: APLv2
Name: classify
Order: "1000"
Prerequisites: drp-community-content:>=4.14.0
Source: RackN
Tags: advanced,enterprise,rackn,classify,linux,library
