## A check that always passes. Can be used as a fallthrough function.
## @name Always
## @test cl_always
## @icon check
function cl_always() {
  echo "pass"
}

## A check that always passes. Can be used as a fallthrough function.
## @name Always
## @test always
## @icon check
## @deprecated Use cl_always instead.
function always() {
  cl_always
}

## @type subnet ^([0-9]{1,3}[.]){3}[0-9]{1,3}[/](([0-9]|[1-2][0-9]|3[0-2]))?$

## Determine if machine in one of given subnets (by ip/cidr). Separate multiple CIDRs with whitespace.
## @name In Subnet
## @test cl_in_subnet(subnet: subnet...)
## @hint subnet 0.0.0.0/32
## @icon code branch
function cl_in_subnet() {
  local _net
  local _cidr
  local _prefix
  local _myaddr
  local _mynet
  local _thenet

  for _cidr in $*
  do
    _prefix=${_cidr#*/}
    _myaddr="{{.Machine.Address}}/$_prefix"

    _mynet=$(ipcalc -n $_myaddr)
    _thenet=$(ipcalc -n $_cidr)

    if [[ "$_mynet" == "$_thenet" ]] ; then
      echo "pass"
    else
      echo "fail"
    fi
  done
}


## Determine if machine in one of given subnets (by ip/cidr). Separate multiple CIDRs with whitespace.
## @name In Subnet
## @test in_subnet(subnet: subnet...)
## @hint subnet 0.0.0.0/32
## @icon code branch
## @deprecated Use cl_in_subnet instead.
function in_subnet() {
  cl_in_subnet "$1"
}

## @type mac ^([0-9A-F]{2}[:-]){5}([0-9A-F]{2})$

## Determine if a machine is in a given subnet (by ip/cidr).
## @name Has Mac
## @test cl_has_mac(mac: mac)
## @hint mac 12:34:56:78:9A:BC
## @icon microchip
function cl_has_mac() {
  # normalize the input MAC for comparison
  local _mac_match=$(echo "$1" | tr -d " :-" | tr '[:upper:]' '[:lower:]')
  cl_get_my_mac    # sets global var MY_MAC_ADDR
  # strip it down to 12 chars (tosses 01 type if exists)
  _mac_match=${_mac_match:(-12)}

  if [[ "$_mac_match" == "$MY_MAC_ADDR" ]]; then
    echo "pass"
  else
    echo "fail"
  fi
}

## Determine if a machine is in a given subnet (by ip/cidr).
## @name Has Mac
## @test has_mac(mac: mac)
## @hint mac 12:34:56:78:9A:BC
## @icon microchip
## @deprecated Use cl_has_mac instead.
function has_mac() {
  cl_has_mac "$1"
}

## Stores the machine's MAC in the "MY_MAC_ADDR" var.
## @name Get My Mac
## @action cl_get_my_mac
## @icon microchip
function cl_get_my_mac() {
  bootif_re='BOOTIF=([^ ]+)'
  local _macaddr="UNKNOWN"
  if [[ $(cat /proc/cmdline) =~ $bootif_re ]]; then
    _macaddr="${BASH_REMATCH[1]//-/:}"
    _macaddr=$(echo $_macaddr | tr -d " :-" | tr '[:upper:]' '[:lower:]')
    _macaddr=${_macaddr:(-12)}
  else
    _macaddr="unknown"
  fi
  MY_MAC_ADDR="$_macaddr"
}

## Stores the machine's MAC in the "MY_MAC_ADDR" var.
## @name Get My Mac
## @action get_my_mac
## @icon microchip
## @deprecated Use cl_get_my_mac instead.
function get_my_mac() {
  cl_get_my_mac
}

# extends Classifier to check if 'inventory-data' field matches value
# This does NOT aggregate because inventory should be per machine

## Checks if the "inventory/data" param matches the value specified.
## This does NOT aggregate because inventory should be per machine.
## @name Has Inventory Value
## @test cl_has_inventory_value(path: string, value: string)
## @icon shopping cart
function cl_has_inventory_value() {
  local _path=$1
  local _value="$(echo -e "$2" | tr -d '[:space:]')"
  local _data="$(drpcli machines get $RS_UUID param "inventory/data")"
  local _jdata="$(jq -r ".[\"$_path\"]" <<< "${_data}" | tr -d '[:space:]')"

  if [[ "$_jdata" == "$_value" ]] ; then
    echo "pass"
  else
    echo "fail"
  fi
}

## Checks if the passed param matches a particular value.
## Complex data structures (lists/arrays and JSON objects) will need careful
## quoting in the value, and formatted to match exactly.
## @name Test Parameter
## @test cl_test_parameter(param: params, value: string)
## @icon pencil
function cl_test_parameter() {
  local _name="${1}"
  # for multi-line output Params, and 'null' type testing later
  local _value="${2}"
  local _fetched_value

  if [[ -z "$_name" || -z "$_value" ]]
  then
    job_error "Missing 'param' or 'value' for test."
    exit 1
  fi

  _fetched_value="$(drpcli machines get $RS_UUID param "$_name" --aggregate | jq -r)"

  if [[ "$_fetched_value" == "${_value}" ]]
  then
    echo "pass"
  else
    echo "fail"
  fi
}
