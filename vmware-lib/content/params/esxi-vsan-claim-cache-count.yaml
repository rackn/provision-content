---
Name: "esxi/vsan-claim-cache-count"
Description: "Defines how many filtered disks to select for VSAN Cache disk claiming"
Documentation: |
  This param defines how many Cache disks to claim from the available list of local
  disks on an ESXI host, after filtering out VSAN non-usable devices.  By default
  only 1 (one) disk will be claimed as a Cache disk for the VSAN volumes.  This
  param allows adjusting the selection list to add more Cache disks.

  Note that the different claiming "rules" may choose to implement this differently.

  In the "simple" disk claiming ruleset, all Boot disks, VSAN, and VMFS claimed disks
  are filtered out.  The list of remaining disks are sorted by size; and then the first
  disk in the list is claimed for Cache disk.

  Note that VSAN can only utilize 1 Cache and 7 Capacity disks as a maximum, per
  VSAN disk group.  This Param is responsible for activating and building multiple
  VSAN disk groups.  The number of Capacity disks in each VSAN disk group can be
  adjusted by using the Param `esxi/vsan-claim-capacity-group-size`.

  In the "simple" claiming ruleset, setting this value to "2" (for example) will select
  the first 2 disks in the filtered list as Cache disks for the VSAND disk groups.
  Presumably these are smaller and faster disk types.

  With 2 Cache disks specified, there will be at most 2 VSAN disk groups created, as
  long as there are 8 or more disks (assuming the disk group size default of 7
  is used).  The code will claim the first 7 disks for the first VSAN disk group,
  then the remainder up to 7 for the second VSAN disk group.  Any remaining
  capacity disks will not be used.

  If there are 17 capacity disks, setting this Param to 3 will result in the following
  disk groups being built (again, assuming disk group size is default of 7):

    * group 1 will have 1 cache and 7 capacity disks
    * group 2 will have 1 cache and 7 capacity disks
    * group 3 will have 1 cache and 3 capacity disks

  The 7 disks capacity limit is a VMware restriction.

  In addition - there can be no more than 5 VSAN disk groups which means that a
  single VSAN host can only support a maximum of 35 Capacity disks, which would
  require 5 Cache disks for a total of 40 VSAN claimed disks.

  !!! note
      This Param will NOT effect VSAN Witness VM disk claiming Cache disk count.
      That must be hard coded to only 1 Cache disk, as such, this Param is
      ignored during VSAN Witness disk claiming process.

Meta:
  color: "green"
  icon: "database"
  title: "RackN Content"
Schema:
  type: "number"
  default: 1
