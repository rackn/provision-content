#!/usr/bin/env bash
# setup the enviornment to execute a 'govc' command

function xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; printf "FATAL: $*\n"; exit $XIT; }

echo ""
echo "==== SETUP govc Environment ===="
echo ""

MSG="Showing GOVC environment variables built from Params ... "

###
#  Connect method allows the task to flexibly switch between GOVC credentials
#  (eg an ESXi Hypervisor host) or a vCenter instance.  This allows a single
#  profile to be carried in a configuration to separate the calls - as a lot of
#  the workflows have to switch back and forth between direct ESXi API and vCenter
#  API connections
###
{{ if .ParamExists "esxi/connect-method" }}CONNECT="{{ .ParamExpand "esxi/connect-method" }}"{{ else }}CONNECT="govc"{{ end }}

###
#  use the GOVC params to define our connection, these can talk to either
#  ESXi hypervisor or to vCenter APIs
###
connect_govc() {
  {{ if .ParamExists "govc/username" -}}
  # username from govc/username
  export GOVC_USERNAME="{{ .Param "govc/username" }}"
  {{ end -}}

  {{ if .ParamExists "esxi/insecure-password" -}}
  # password from esxi/insecure-password
  export GOVC_PASSWORD="{{ .Param "esxi/insecure-password" }}"
  {{ else -}}
  # password from govc/password
  export GOVC_PASSWORD="{{ .Param "govc/password" }}"
  {{ end -}}

  {{ if .ParamExists "govc/url" -}}
  export GOVC_URL="{{ .ParamExpand "govc/url" }}"
  {{ end -}}

  {{ if ( .Param "govc/port" ) }}export GOVC_URL="${GOVC_URL}:{{ .Param "govc/port" | toString }}"{{ end }}
  {{ if ( .ParamExists "govc/insecure" ) }}export GOVC_INSECURE={{ if eq (.Param "govc/insecure") true }}1{{ else }}0{{ end }}{{ end }}
} # end connect_govc()

# use the vcenter/* Params to define our connection - only used explicitly
# for calls to a vCenter instance - this mechanism will fallback to using
# the GOVC values to support the original use case, however, the "govc about"
# test API call will be parsed for vCenter API connection, and fail out if
# we are not talking to a vCenter host
connect_vcenter() {
  {{ if .ParamExists "vcenter/username" -}}
  # username from vcenter/username
  export GOVC_USERNAME="{{ .Param "vcenter/username" }}"
  {{ end -}}

  {{ if .ParamExists "vcenter/password" -}}
  # password from vcenter/password
  export GOVC_PASSWORD="{{ .Param "vcenter/password" }}"
  {{ else -}}
  # password from govc/password
  export GOVC_PASSWORD="{{ .Param "govc/password" }}"
  {{ end -}}

  {{ if .ParamExists "vcenter/url" -}}
  export GOVC_URL="{{ .ParamExpand "vcenter/url" }}"
  {{ end -}}

  {{ if ( .Param "vcenter/port" ) }}export GOVC_URL="${GOVC_URL}:{{ .Param "vcenter/port" | toString }}"{{ end }}
  {{ if ( .ParamExists "vcenter/insecure" ) }}export GOVC_INSECURE={{ if eq (.Param "vcenter/insecure") true }}1{{ else }}0{{ end }}{{ end }}
} # end connect_vcenter()

###
#  Requires ABOUT to have a 'govc about' connection set of values
#  Requires CONNECT to be one of the supported connection types (eg "vcenter")
#  if CONNECT type is "govc" no explicit connection enforcement is
#  performed (due backwards compatibility)
#    * some returned values
#      vcenter (vcsa):
#         OS type:      linux-x64
#         API type:     VirtualCenter
#         Product ID:   vpx
#      esxi hypervisor (bare metal - not Witness VM):
#         OS type:      vmnix-x86
#         API type:     HostAgent
#         Product ID:   embeddedEsx
###
validate_connection() {
  local _got="$(echo "$ABOUT" | grep "^API type:" | cut -d ":" -f2 | sed 's/^[   ]*//;s/[    ]*$//')"
  local _fail="Invalid connection type, got '$CONNECT' -  expect to connect to"
  local _err _msg

  case $CONNECT in
    esxi)    _msg="'HostAgent' (esxi hypervisor)" 
             [[ "$_got" != "HostAgent" ]] && _err="true" || true
      ;;
    vcenter) _msg="'VirtualCenter' (vCenter, VCSA)" 
             [[ "$_got" != "VirtualCenter" ]] && _err="true" || true
      ;;
    govc)    _msg="not validated - could be either ESXi or vCenter (got '$_got')"
             _err=""
      ;;
    *)       _msg="Failed validation connection type (expect 'HostAgent' or 'VirtualCenter')"
             _err="true"
      ;;
  esac

  if [[ -z "$_err" ]]
  then
    echo ">>> Validating host connection API Type requested for '$CONNECT'"
    echo ">>> SUCCEEDED - API type got '$_got' matching to $_msg"
  else
    echo "!!! FAILED: expect a connect type of '$CONNECT', got '$_got'"
    xiterr 1 "$_fail $_msg" 
  fi
} # end validate_connection()

###
#  Setup our connection variables.  legacy is "GOVC" which does not
#  distinguish between ESXi or vCenter.  "vcenter" utilizes specific
#  param values separate from the govc/* connection Params.  CONNECT
#  type of "esxi" not yet used in code, but available for validation
#  for later code use.
###
case $CONNECT in
  esxi)     connect_govc    ;;
  govc)     connect_govc    ;;
  vcenter)  connect_govc         # start with 'govc/*' then override with 'vcenter/*'
            connect_vcenter ;;
  *)        xiterr 1 "Connection type requested unsupported method (expec 'govc' or 'vcenter')" ;;
esac

# use GOVC values if set - no comparable vcenter/* ones
{{ if ( .Param "govc/datastore" )       }}export GOVC_DATASTORE="{{ .ParamExpand "govc/datastore" }}"{{ end }}
{{ if ( .Param "govc/network" )         }}export GOVC_NETWORK="{{ .Param "govc/network" }}"{{ end }}
{{ if ( .Param "govc/resource-pool" )   }}export GOVC_RESOURCE_POOL="{{ .Param "govc/resource-pool" }}"{{ end }}

# if GOVC_URL is empty, we check to see if we are running in a context
# off of a Machine, and set that here to the Machine name
if [[ -z "$GOVC_URL" ]]
then
  export GOVC_URL="{{ .Machine.Name }}"
  WARN="GOVC_URL is empty; assuming self Machine name ('$GOVC_URL') - hope that's what you want"
fi
[[ -z "$GOVC_USERNAME" ]] && ERR="required setting GOVC_PASSWORD is empty (param: 'govc/password')"
[[ -z "$GOVC_PASSWORD" ]] && ERR="$ERR :: required setting GOVC_USERNAME is empty (param: 'govc/username')"

[[ -n "$ERR" ]] && xiterr 1 "$ERR"
[[ -n "$WARN" ]] && echo "WARNING: $WARN"

if [[ "{{ .Param "rs-debug-enable" }}" == "true" ]]
then
  echo "WARNING: GOVC_PASSWORD is visible since 'rs-debug-enable' is set to 'true'."
  echo ""
  echo "$MSG"
  env | grep -v grep | grep "GOVC_"
else
  echo "NOTICE: GOVC_PASSWORD is obfuscated.  Set 'rs-debug-enable' to 'true' to view it on next run."
  echo ""
  echo "$MSG"
  env | grep -v grep | grep "GOVC_" | sed 's/^\(.*PASSWORD=\).*$/\1*** obfuscated ***/'
fi

echo ">>> Starting API health check request to '$GOVC_URL'"
# govc is so stupid it doesn't know how to exit with error if this fails
ABOUT=$(govc about)

if [[ -n "$ABOUT" ]]
then
  echo "Connection Information (from 'govc about'):"
  echo "$ABOUT"
  echo ""
  validate_connection
else
  echo "!!! Host API service check to '$GOVC_URL' failed; exit with error"
  exit 1
fi
