
This content bundle provides comprehensive tooling for deploying and managing HAProxy
load balancers in both standalone and clustered configurations.

## Design Philosophy

The content bundle follows a flexible, infrastructure-as-code approach with two key design principles:

1. **Auto Discovery**: The primary design leverages "Filter objects" stored in the 
    `haproxy/filters` parameter to discover backend nodes. These nodes are then used 
    to automatically generate the `haproxy.cfg` file. This approach enables dynamic,
    scalable load balancer configurations.

2. **Manual Override**: For scenarios requiring precise control, the bundle supports
    explicit configuration through override parameters, bypassing the auto-discovery
    mechanism.

## Deployment Methods

### 1. Auto Discovery Mode
Deploy using built-in service discovery:
- Set `haproxy/role` on nodes to define their roles
- Define services using `haproxy/backend/services` on backend nodes
- Configure discovery using `haproxy/filters` on load balancer nodes
- The system automatically generates frontend and backend maps
- Configuration rendered through the template system

### 2. Manual Override Mode
Deploy using explicit configuration:
- Set `haproxy/filters` to an empty array
- Define `haproxy/frontend/map-override` and `haproxy/backend/map-override`
- Configuration renders using your explicit settings

### Auto Discovery Example

This is a minimal cluster profile that leverages AutoDiscovery, with optional 
`haproxy/backend/config` and `haproxy/frontend/config` parameters:

```yaml
Params:
  universal/application: my-ha-cluster
  universal/workflow-chain-index-override: cluster
  on-complete-work-order-mode: true
  cluster/wait-for-members: false
  cluster/machine-types:
    - load-balancer
    - backend
  cluster/machines:
    load-balancer:
      pipeline: universal-application-haproxy-server
      names: ['my-ha-proxy.local.domain']
      Params:
        haproxy/role: load-balancer
        haproxy/filters:
          - Profiles=Eq(universal-application-apache-web-server) Params.haproxy/role=Eq(backend) Profiles=Eq({{ .Machine.Name }})
        haproxy/frontend/config:  # Optional
          http:
            mode: http
        haproxy/backend/config:  # Optional
          http:
            mode: http
            balance: roundrobin
    web-backend:
      pipeline: universal-application-apache-web-server
      names: ['web01.local.domain', 'web02.local.domain']
      Params:
        haproxy/role: backend
        haproxy/backend/services:
          http: 80
```

### Manual Override Example

This is a minimal example of a manual override deployment, with overridden
config file. These profiles and params would be assigned to a machine or a
profile that is assigned to a machine.

```yaml
Profiles:
  - universal-application-haproxy-server
Params:
  haproxy/config-template: my-haproxy.cfg.tmpl
  haproxy/frontend/map-override:
    http:
      http: 80
      ssl: [443, 80443]
  haproxy/backend/map-override:
    http:
      - server d52-54-00-f9-6c-09.example.com 192.168.122.13:80 maxconn 32
      - server d52-54-00-08-5a-04.example.com 192.168.122.14:80 maxconn 32
      - server d52-54-00-96-b2-4a.example.com 192.168.122.15:80 maxconn 32
```

### Cluster Pipeline Example

This is a minimal example cluster universal-application. This example is used
to define a load balancer to multiple generic apache web backend using roundrobin
load balancing. This configuration will provision 3 machines:

  - `my-ha-proxy.local.domain` (load-balancer)
  - `web01.local.domain` (apache web-backend)
  - `web02.local.domain` (apache web-backend)

The configuration from the previous step is applied within the `cluster/machines.load-balancer`
section, which is copied to a machine when the cluster is provisioned.

```yaml
Params:
  universal/application: my-ha-cluster
  universal/workflow-chain-index-override: cluster
  on-complete-work-order-mode: true
  cluster/wait-for-members: false
  cluster/machine-types:
    - load-balancer
    - backend
  cluster/machines:
    load-balancer:
      pipeline: universal-application-haproxy-server
      names: ['my-ha-proxy.local.domain']
      Params:
        haproxy/role: load-balancer
        haproxy/filters:
          - Profiles=Eq(universal-application-apache-web-server) Params.haproxy/role=Eq(backend) Profiles=Eq({{ .Machine.Name }})
        haproxy/frontend/config:
          http:
            mode: http
        haproxy/backend/config:
          http:
            mode: http
            balance: roundrobin
    web-backend:
      pipeline: universal-application-apache-web-server
      names: ['web01.local.domain', 'web02.local.domain']
      Params:
        haproxy/role: backend
        haproxy/backend/services:
          http: 80
```

## Configuration Customization

The bundle supports full customization of the HAProxy configuration:
- Override the config template using `haproxy/config-template`
- Customize frontend settings with `haproxy/frontend/config`
- Customize backend settings with `haproxy/backend/config`
- Define per-service configuration with `haproxy/backend/services/config`

## Requirements

- Digital Rebar Provision v4.14.0 or later
- DRP Community Content Bundle
- Either Alma Linux 9 or VMware Photon 5 for target nodes
- Network access to package repositories
- Appropriate permissions for service management

## Getting Started

1. Choose your deployment method (Auto Discovery or Manual Override)
2. Apply the universal-application-haproxy-server profile
3. Configure required parameters
4. Run the workflow

For detailed parameter descriptions and examples, refer to the individual
component documentation.
