#!/usr/bin/env bash

{{ template "setup.tmpl" . }}

# Helper functions

# Store the original job_error function
eval "original_job_error() { $(declare -f job_error | tail -n +2) }"

# Redefine job_error so that it `exit 1` when called
function job_Error() {
  original_job_error "$@"
  exit 1
}

# Cache for SELinux enabled status
SELINUX_ENABLED=""
# Check if SELinux is enabled
selinux_enabled() {
  if [ -z "${SELINUX_ENABLED}" ]; then
    command_exists selinuxenabled && selinuxenabled && SELINUX_ENABLED=true || SELINUX_ENABLED=false
  fi
  ${SELINUX_ENABLED}
}

# Cache for firewall type
FIREWALL_TYPE=""
# Determine the firewall type (firewalld, iptables, or ebtables)
firewall_type() {
  if [ -z "${FIREWALL_TYPE}" ]; then
    if command_exists firewall-cmd; then
      FIREWALL_TYPE='firewalld'
    elif command_exists iptables; then
      FIREWALL_TYPE='iptables'
    elif command_exists ebtables; then
      FIREWALL_TYPE='ebtables'
    else
      job_error 'Unknown firewall type'
    fi
  fi
  echo "${FIREWALL_TYPE}"
}

# Cache for firewall enabled status
FIREWALL_ENABLED=""
# Check if the firewall is enabled
firewall_enabled() {
  if [ -z "${FIREWALL_ENABLED}" ]; then
    local fw_type=$(firewall_type)
    local check_cmd=""
    case "${fw_type}" in
      firewalld) check_cmd="firewall-cmd --state" ;;
      iptables)  check_cmd="iptables -L" ;;
      ebtables)  check_cmd="ebtables -L" ;;
      *)         FIREWALL_ENABLED=false; return ;;
    esac
    ${check_cmd} >/dev/null 2>&1 && FIREWALL_ENABLED=true || FIREWALL_ENABLED=false
  fi
  ${FIREWALL_ENABLED}
}

# Ensure a firewall rule for a port doesn't exist, then create it if necessary
add_firewall_rule() {
  local port="$1"
  case "$(firewall_type)" in
    firewalld)
      firewall-cmd --permanent --zone=public --query-port="${port}"/tcp >/dev/null 2>&1 || 
        firewall-cmd --permanent --zone=public --add-port="${port}"/tcp || 
        job_error "Failed to add firewall rule for port ${port}"
      ;;
    iptables)
      iptables -C INPUT -p tcp --dport "${port}" -j ACCEPT 2>/dev/null ||
        iptables -A INPUT -p tcp --dport "${port}" -j ACCEPT || 
        job_error "Failed to add iptables rule for port ${port}"
      ;;
    ebtables)
      ebtables -L INPUT | grep -q -- "-p tcp --dport ${port} -j ACCEPT" ||
        ebtables -A INPUT -p tcp --dport "${port}" -j ACCEPT || 
        job_error "Failed to add ebtables rule for port ${port}"
      ;;
  esac
}

# Restart the firewall service
restart_firewall() {
  local fw_type=$(firewall_type)
  local restart_cmd=""
  case "${fw_type}" in
    firewalld) restart_cmd="firewall-cmd --reload" ;;
    iptables)  restart_cmd="service iptables restart" ;;
    ebtables)  restart_cmd="service ebtables restart" ;;
    *)         job_error "Cannot restart unknown firewall type: ${fw_type}"; return 1 ;;
  esac
  ${restart_cmd} || job_error "Failed to restart ${fw_type}"
}

# Main script

# Check if haproxy/frontend/map is empty
if [ "$(drpcli machines get {{ .Machine.UUID }} param haproxy/frontend/map)" = "{}" ]; then
  job_success "No services found in haproxy/frontend/map"
fi

job_info "Detected firewall type: $(firewall_type)"

if firewall_enabled; then
  job_info 'Firewall is enabled.'
else
  job_info 'Firewall is not enabled or not available.'
fi

if selinux_enabled; then
  job_info 'SELinux is enabled.'
  job_info 'Installing policycoreutils-python-utils'
  install_lookup policycoreutils-python-utils
else
  job_info 'SELinux is not installed, not enabled, or the selinuxenabled command is not available.'
fi

firewall_changed=false

# Process each service and port
drpcli machines get {{ .Machine.UUID }} param haproxy/frontend/map | jq -r 'to_entries[] | "\(.key) \(.value)"' | while read -r service port; do
  job_info "Processing service ${service} with port ${port}"
  
  if firewall_enabled; then
    job_info "Adding firewall rule for port: ${port}"
    add_firewall_rule "${port}"
    firewall_changed=true
  fi

  if selinux_enabled; then
    job_info "Running semanage commands for port: ${port}"
    semanage port -a -t http_port_t -p tcp "${port}" || semanage port -m -t http_port_t -p tcp "${port}" || job_error "Failed to add/modify SELinux port ${port}"
  fi
done || job_error "Failed to process haproxy/frontend/map"

if $firewall_changed; then
  job_info 'Restarting firewall...'
  restart_firewall
fi

job_success "${0}: Completed Successfully"
