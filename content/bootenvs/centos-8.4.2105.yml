---
Name: "centos-8.4.2105-install"
Description: "CentOS-8.4.2105 installer that points to the latest CentOS 8 release."
Documentation: |
  This BootEnv installs the CentOS 8.4.2105 DVD operating system.  The x86_64
  (amd64), aarch64 (ARM 64), and ppc64le (PowerPC 64 LE) architectures are
  supported.

  ISOs can be downloaded from:

    * <http://isoredirect.centos.org/centos>

Meta:
  type: "os"
  feature-flags: "change-stage-v2"
  icon: "linux"
  color: "blue"
  title: "Digital Rebar Community Content"
  group-by: CentOS
Loaders:
  amd64-uefi: EFI/BOOT/BOOTX64.EFI
  arm64-uefi: EFI/BOOT/BOOTAA64.EFI
OS:
  Family: "redhat"
  Version: "8"
  Name: "centos-8.4.2105"
  SupportedArchitectures:
    x86_64:
      IsoFile: "CentOS-8.4.2105-x86_64-dvd1.iso"
      IsoUrl: "https://rackn-repo.s3.amazonaws.com/isos/centos/8/CentOS-8.4.2105-x86_64-dvd1.iso"
      Sha256: "0394ecfa994db75efc1413207d2e5ac67af4f6685b3b896e2837c682221fd6b2"
      Kernel: "images/pxeboot/vmlinuz"
      Initrds:
        - "images/pxeboot/initrd.img"
      BootParams: >-
        ksdevice=bootif
        ks={{.Machine.Url}}/compute.ks
        method={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    aarch64:
      Loader: "grubarm64.efi"
      IsoFile: "CentOS-8.4.2105-aarch64-dvd1.iso"
      IsoUrl: "https://rackn-repo.s3.amazonaws.com/isos/centos/8/CentOS-8.4.2105-aarch64-dvd1.iso"
      Sha256: "6654112602beec7f6b5c134f28cf6b77aedc05b2a7ece2656dacf477f77c81df"
      Kernel: "ppc/ppc64/vmlinuz"
      Initrds:
        - "ppc/ppc64/initrd.img"
      BootParams: >-
        ksdevice=bootif
        ks={{.Machine.Url}}/compute.ks
        method={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
    ppc64le:
      Loader: "core.elf"
      IsoFile: "CentOS-8.4.2105-ppc64le-dvd1.iso"
      IsoUrl: "https://rackn-repo.s3.amazonaws.com/isos/centos/8/CentOS-8.4.2105-ppc64le-dvd1.iso"
      Sha256: "9cfca292a59a45bdb1737019a6ac0383e0a674a415e7c0634262d66884a47d01"
      Kernel: "ppc/ppc64/vmlinuz"
      Initrds:
        - "ppc/ppc64/initrd.img"
      BootParams: >-
        ksdevice=bootif
        ks={{.Machine.Url}}/compute.ks
        method={{.Env.InstallUrl}}
        inst.geoloc=0
        {{.ParamExpand "kernel-options"}}
        --
        {{.ParamExpand "init-options"}}
RequiredParams:
OptionalParams:
  - "operating-system-disk"
  - "provisioner-default-password-hash"
  - "kernel-console"
  - "kernel-options"
  - "proxy-servers"
  - "select-kickseed"
  - "init-options"
  - "kickstart-base-packages"
  - "extra-packages"
Templates:
  - Name: "kexec"
    ID: "kexec.tmpl"
    Path: "{{.Machine.Path}}/kexec"
  - ID: "default-pxelinux.tmpl"
    Name: "pxelinux"
    Path: "pxelinux.cfg/{{.Machine.HexAddress}}"
  - ID: "default-ipxe.tmpl"
    Name: "ipxe"
    Path: "{{.Machine.Address}}.ipxe"
  - ID: "default-pxelinux.tmpl"
    Name: "pxelinux-mac"
    Path: 'pxelinux.cfg/{{.Machine.MacAddr "pxelinux"}}'
  - ID: "default-ipxe.tmpl"
    Name: "ipxe-mac"
    Path: '{{.Machine.MacAddr "ipxe"}}.ipxe'
  - ID: "default-grub.tmpl"
    Name: "grub"
    Path: "grub/{{.Machine.Address}}.cfg"
  - ID: "default-grub.tmpl"
    Name: "grub-mac"
    Path: 'grub/{{.Machine.MacAddr "grub"}}.cfg'
  - Name: grub-http-boot
    Path: '{{.Env.PathFor "tftp" ""}}/EFI/BOOT/grub.cfg'
    ID: default-grub.tmpl
  - ID: "select-kickseed.tmpl"
    Name: "compute.ks"
    Path: "{{.Machine.Path}}/compute.ks"
