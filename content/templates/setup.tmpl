#!/usr/local/bin/env bash
# Common prelude templated bash scripts that are not tasks.
#
###
#  This is a BASH script snippet intended to be run inside other BASH templates.
#
#  Simple helper to prep a system with DRPCLI and JQ.  If not already installed,
#  download and install the `drpcli` and `jq` binaries in /usr/local/bin and then
#  source our `helper` tools
#
#  To use this in other templates, simply specify:
#
#         \{\{template "setup.tmpl" .\}\}
#
#  without the backslashes.
###

set -e

# We pretty much always need these parameters set, but
# don't overwrite them if they already exist.
[[ $RS_TOKEN ]] || export RS_TOKEN="{{.GenerateInfiniteMachineToken}}"
[[ $RS_ENDPOINT ]] || export RS_ENDPOINT="{{.ApiURL}}"
[[ $RS_UUID ]] || export RS_UUID="{{.Machine.UUID}}"
[[ $RS_MC_ROLE ]] || export RS_MC_ROLE="{{get .Machine.Meta "machine-role"}}"
[[ $RS_MC_TYPE ]] || export RS_MC_TYPE="{{if .ParamExists "machine/type"}}{{.Param "machine/type"}}{{else}}undefined{{end}}"
[[ $RS_MC_NAME ]] || export RS_MC_NAME="{{.Machine.Name}}"
[[ $HOME ]] || export HOME=$(pwd)
###
#  if we want debugging of our scripts, set the Param to true
#  also set shell variable for script reuse if desired for further
#  debugging
#  for security, this can be disabled universally by setting security/debug-block
###
{{ if .ParamExists "rs-debug-enable" }}
  {{ if .Param "rs-debug-enable" }}
    {{ if .ParamExists "security/debug-block"}}
      {{ if .Param "security/debug-block" }}
      echo "NOTE: secury/debug-block enabled: overriding rs-debug-enable on machine"
      drpcli machines set $RS_UUID param rs-debug-enable to false > /dev/null
      {{ end }}
    {{ else }}
      # use in shell as: [[ $RS_DEBUG_ENABLE ]] && echo "debugging"
      RS_DEBUG_ENABLE="{{.Param "rs-debug-enable"}}"
      set -x
    {{ end }}
  {{ end }}
{{ end }}

function xiterr() { [[ $1 =~ ^[0-9]+$ ]] && { XIT=$1; shift; } || XIT=1; echo "FATAL: $*";  __exit $XIT; }

function fixup_path() {
  local _add_path
  for _add_path in $(echo $* | sed 's/[:,]/ /g')
  do
    mkdir -p $_add_path
    echo ":$PATH:" | grep -q ":$_add_path" || export PATH="$PATH:$_add_path"
  done
}

# Take a mapping of OS packages, and alter it to  to install based on various factors.
cat > install.packages <<EOF
{{ .ParamComposeExpand "install-os-pkg-map" | toJson }}
EOF
INSTALL_PKGS="$(pwd)/install.packages"

# Given a list of packages, alter the name of the package based on the 
# parameter "install-os-pkg-map", and install them. This allows, for example, the package
# "ansible" to be translated into "ansible-core" for different distributions.
# Lookup translation based upon:
#   * OS_NAME
#   * OS_NAME with less dots
#   * OS_TYPE
#   * OS_FAMILY
install_lookup() {
    local to_install=()
    local package
    local new_package
    for package in "$@"; do
        new_package=$(jq -r ".[\"$package\"][\"$OS_NAME\"]" $INSTALL_PKGS)
        if [[ "$new_package" == "null" ]] ; then
            new_package=$(jq -r ".[\"$package\"][\"${OS_NAME%.*}\"]" $INSTALL_PKGS)
        fi
        if [[ "$new_package" == "null" ]] ; then
            new_package=$(jq -r ".[\"$package\"][\"${OS_TYPE}\"]" $INSTALL_PKGS)
        fi
        if [[ "$new_package" == "null" ]] ; then
            new_package=$(jq -r ".[\"$package\"][\"${OS_FAMILY}\"]" $INSTALL_PKGS)
        fi
        if [[ "$new_package" == "null" ]] ; then
            new_package=$package
        fi

        to_install+=("$new_package")
    done

    install "${to_install[@]}"
}


{{ template "task-helpers.tmpl" . }}

fixup_path /usr/local/bin /usr/sbin /sbin /opt/bin

arch=$(uname -m)
case $arch in
    x86_64|amd64) arch="amd64";;
    arm64|aarch64) arch="arm64";;
    ppc64le) arch="ppc64le";;
    *)
        xiterr 1 "Unknown arch $(uname -m)";;
esac

osfamily=$(grep "^ID=" /etc/os-release | tr -d '"' | cut -d '=' -f 2)
osversion=$(grep "^VERSION_ID=" /etc/os-release | tr -d '"' | cut -d '=' -f 2)

osclass="linux"
case $osfamily in
    mac*|dar*) osclass="darwin";;
    win*) osclass="windows";;
    *) true;;
esac

INSTALL_DIR=/usr/local/bin
if grep -q coreos /etc/os-release ; then
  INSTALL_DIR=/opt/bin
elif grep -q alpine /etc/os-release ; then
  INSTALL_DIR=/usr/bin
fi
mkdir -p $INSTALL_DIR

if ! command -v drpcli > /dev/null 2>&1 ; then
    echo "Installing drpcli in $INSTALL_DIR"
    curl --noproxy '*' -gsfLo "$INSTALL_DIR/drpcli" "{{.ProvisionerURL}}/files/drpcli.$arch.$osclass"
    chmod 755 "$INSTALL_DIR/drpcli"
else
    INSTALL_DIR="$(dirname $(command -v drpcli))"
fi

if [[ ! -e $INSTALL_DIR/drpjq ]] ; then
    ln -s $INSTALL_DIR/drpcli $INSTALL_DIR/drpjq
fi
if [[ ! -e $INSTALL_DIR/jq ]] ; then
    ln -s $INSTALL_DIR/drpcli $INSTALL_DIR/jq
fi
unset INSTALL_DIR

# Helper is rendered by the agent, and includes additional helpful functions; for
# source, see repo https://gitlab.com/rackn/provision and file agent/cmdHelper.go
if [[ -r ./helper ]]; then
    . ./helper
    __sane_exit
fi
