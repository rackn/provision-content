#!/bin/bash
# Copyright 2017-2021, RackN
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# We get the following variables from start-up.sh
# MAC BOOTDEV ADMIN_IP DOMAIN HOSTNAME HOSTNAME_MAC default_host
shopt -s extglob
export PS4="${BASH_SOURCE}@${LINENO}(${FUNCNAME[0]}): "
cp /usr/share/zoneinfo/GMT /etc/localtime
# Set up for the agent
mkdir -p /var/lib/drp-agent/
curl -fgLko /var/lib/drp-agent/agent-cfg.yml "{{.SecureProvisionerURL}}/{{.Machine.Path}}/agent-cfg.yml"
cat >/etc/sysconfig/drp-agent <<EOF
IP=$IP
EOF
drpcli agent install

# This will contain a token appropriate for the path being
# used below.  Reset the token to the longer machine token.
export RS_TOKEN="{{.GenerateInfiniteMachineToken}}"

json="$(drpcli machines show $RS_UUID --slim Params,Meta)"
# If we did not get a hostname from DHCP, get it from DigitalRebar Provision.
if [[ $default_host ]]; then
    HOSTNAME="$(jq -r '.Name' <<< "$json")"
fi
if [ -f /etc/sysconfig/network ] ; then
    sed -i -e "s/HOSTNAME=.*/HOSTNAME=${HOSTNAME}/" /etc/sysconfig/network
fi
echo "${HOSTNAME#*.}" >/etc/domainname
hostname "$HOSTNAME"

{{template "reset-workflow.tmpl" .}}
drpcli agent start
echo "Follow along with job logs using journalctl -u drp-agent"

# The last line in this script must always be exit 0!!
exit 0
