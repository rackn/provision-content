---
Name: proxmox/storage-config
Description: The configuration for Storage backends in Proxmox.
Documentation: |
  This param is used to define the configuration for the various
  backend storage on a Proxmox host.

  The Param is an object, with a Key for the type of storage, based
  on supported setup configurations in Tasks, following with another
  key that is specific to the given instace configuration.  This allows
  for support of multiple objects of the same type.

    ```yaml
    lvmthin:
      local-lvm:
        device: /dev/sdb
        vgname: pve
        thinpool: data
        content: rootdir,images
        size: 95%FREE
        maxfiles: "7"
    dir:
      local-images:
        path: /var/lib/images
        content: images,iso
      local:
        path: /var/lib/vz
        content: iso,vztmpl,backup
        format: qcow2,vmdk
      backup:
        path: /mnt/backup
        content: backup
        prune-backups: keep-all=0,keep-daily=7,keep-hourly=24
    ```

  !!! warning:: You may not specify a `dir` type with *name* of `local`, this is a reserved
               Dir type of storage.  Doing so will cause a fatal error in the Workflow and
               stop workflow processing.

  The following types have not yet been implemented yet.

    * lvm, zfspool, btrfs, nfs, cifs, pbs, glusterfs, cephfs, iscsi, iscsidirect, rbd, zfs

  For Storage configuration details, see the Proxmox documentation at:

    * https://pve.proxmox.com/wiki/Storage

  !!! note
      This object type does not yet contain a Schema for validation of the configuration.
      Field values in each segment directly map to Proxmox Storage configuration directives,
      use the documentation for guidelines, and follow the example defined above.  However,
      some values may be helpers specific to a task (eg `device: /dev/sdb`, defines to
      create the LVM Thinpool using the backing Device specified).

  A future implementation example for ZFS Pool configuration might look like:

    ```yaml
    zfspool:
      local-zfs:
        pool: rpool/data
        sparse: true
        content: images,rootdir
    ```

  If no values are specified (the default), then the product default of LVM-Thin type storage
  will be set up based on the `proxmox/storage-device` and `proxmox/storage-name` Param
  settings.

Meta:
  color: blue
  icon: database
  title: Digital Rebar Community Content
Secure: false
Schema:
  type: object
  default: {}
