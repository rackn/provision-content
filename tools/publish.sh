#!/usr/bin/env bash

set -e

[[ $GOPATH ]] || export GOPATH="$HOME/go"
fgrep -q "$GOPATH/bin" <<< "$PATH" || export PATH="$PATH:$GOPATH/bin"

go install github.com/stevenroose/remarshal@latest

version=$(tools/version.sh)
. tools/version.sh
branch=${MajorV}.${MinorV}

tools/pieces.sh | while read i ; do
    echo "Publishing $i to rebar-catalog"
    remarshal -i $i.yaml -o $i.json -if yaml -of json
    mkdir -p rebar-catalog/$i
    cp $i.json rebar-catalog/$i/$version.json
    echo "Publishing docs"
    mkdir -p rebar-catalog/docs/$branch
    cp $i.rst rebar-catalog/docs/$branch/$i.rst
done

