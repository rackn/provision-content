Overview
========

This Digital Rebar Provision (DRP) content bundle sets up a Nomad
service on a host.

Currently only Linux or MacOS (darwin) based systems are supported.
Future implementations should include Windows or other OSes via
an appropriate installer script.

Additionally, many future implementation goals will be considered
for this content bundle.  For example, setting up HA clustering
for Nomad services.


