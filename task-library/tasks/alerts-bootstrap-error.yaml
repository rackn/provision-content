---
Name: alerts-bootstrap-error
Description: Creates a critical alert if a bootstrap Task failure occurs.
Documentation: |
  This task should be set to run when any of the important bootstrapping
  tasks fail.  This task then raises an Alert on the system to help improve
  visibility of a failed self bootstrap error condition.

ExtraClaims:
  - scope: "alerts"
    action: "create"
    specific: "*"
Meta:
  color: red
  feature-flags: sane-exit-codes
  icon: alarm
  title: RackN
Templates:
  - Name: alerts-bootstrap-error.sh
    Contents: |
      #!/usr/bin/env bash
      # Raise an Alert on a Bootstrap Task failure

      set -e
      {{ if ( .Param "rs-debug-enable") }}set -x{{ end }}

      {{ if .Param "alerts-bootstrap-handled" }}
      echo ""
      echo "Retrying the original task to see if it is corrected."
      echo ""
      drpcli {{.Machine.Prefix}} remove $RS_UUID param alerts-bootstrap-handled >/dev/null
      exit 0
      {{ end }}

      LEVEL="ERROR"
      PREFIX="Critical"

      {{ $error_stack := index .Machine.TaskErrorStacks 0 -}}
      {{ $id := $error_stack.CurrentTask -}}
      TASK="{{ index $error_stack.TaskList $id }}"

      echo ""
      echo "Bootstrap task hit an error in Task: '$TASK'"
      echo "Please correct the bootstrapping failure problem, and then"
      echo "restart the Workflow on the self-runner machine."
      echo ""

      {{ $job := $error_stack.CurrentJob -}}
      {{ if $job -}}
      JOB_PARAM='alert/job: {{ $job }}'
      {{ else -}}
      JOB_PARAM=""
      echo "DRP Endpoint version does not support TaskErrorStacks[0].CurrentTask."
      echo "Upgrade to DRP Endpoint v4.11.0 or newer for CurrentTask reference."
      {{ end -}}
      NAME="{{ .Machine.Name }}"
      UUID="{{ .Machine.Uuid }}"
      SOURCE="{{ index .Machine.Tasks .Machine.CurrentTask }}"

      cat << EOF > alert.yaml
      Name: "Bootstrap failure on {{ .Machine.Name }}"
      Level: "$LEVEL"
      Params:
        alert/machine: "$NAME"
        alert/content: "System Bootstrap Error"
        alert/level: "$LEVEL"
        alert/source: "$SOURCE"
        alert/task: "$TASK"
        $JOB_PARAM
      Contents: |
        $PREFIX

        A bootstrap task failed to execute correctly.  This DRP Endpoint system is
        in an inconsistent state that should not be used until the failed task is
        corrected, and bootstrap successfully completes.

        Machine Name: "$NAME"
        Machine UUID: "ux://machines/$UUID"
        Task that errored:  "ux://tasks/$TASK"
        Job log of errored task:  "ux://jobs/{{ $job }}"

        The Machine complete Task list at the time of the error was:
        ------------------------------------------------------------
        $(echo '{{ $error_stack | toPrettyJson }}' | tr -d '",{}[]')
        ------------------------------------------------------------
      EOF

      {{ if ( has "alerts" .Info.Features ) -}}
      ALERT_ID=$(drpcli alerts create --unique alert.yaml | jq -r '.Uuid')
      echo ">>> More complete details can be found in Alert ID: 'ux://alerts/$ALERT_ID'"
      {{ else -}}
      echo ">>> WARNING:  This DRP Endpoint does not support the 'alerts' feature, no"
      echo "              higly visible alert will be raised with details."
      echo ""
      echo "ALERT details would have been created with the following YAML alert:"
      echo "===================================================================="
      cat alert.yaml
      {{ end -}}

      rm -f alert.yaml

      echo ""
      echo "NOTICE:  This task intentionally exits with error code 1, this"
      echo "         is by design for the Task Error Handler process, an exit"
      echo "         of 0 means the original problem has been detected as having"
      echo "         been corrected."
      echo ""

      drpcli {{.Machine.Prefix}} set $RS_UUID param alerts-bootstrap-handled to true >/dev/null

      exit 1
