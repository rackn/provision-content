---
Name: "dr-server-build-airgap-bundle"
Description: "Builds the DRP Airgap bundle"
Documentation: |
  Builds a DRP airgap bundle.

  The primary use cases for this task are

    1. drp-server pipline

  For operators, this feature makes it easy to build new bundles with each release.

  If only one version is required for download, use the `dr-server/initial-version` param.

  If you want multiple versions to be downloaded in the bundle, use the `dr-server/airgap-versions` param.

  Downloaded version defaults to stable.

  The bundle will be stored to the location provided by `dr-server/airgap-file-url`

ExtraClaims:
  - scope: "files"
    action: "*"
    specific: "*"
  - scope: "contents"
    action: "*"
    specific: "rackn-license"
  - scope: "endpoints"
    action: "*"
    specific: "*"
  - scope: "alerts"
    action: "create"
    specific: "*"
  - scope: "profiles"
    action: "update"
    specific: "global"
OptionalParams:
  - "dr-server/airgap-file-upload-location"
  - "dr-server/airgap-file-upload-server-path"
  - "dr-server/initial-version"
  - "dr-server/airgap-versions"
  - "dr-server/airgap-iso-bootenvs"
  - "dr-server/airgap-iso-profiles"
  - "dr-server/airgap-platform"
  - "dr-server/airgap-catalog"
Templates:
  - Name: "build-airgap-bundle"
    Contents: |-
      #!/usr/bin/env bash

      set -e

      # Include required templates
      {{ template "setup.tmpl" .}}
      {{ template "download-tools.tmpl" . }}

      construct_base_command() {
        # Construct the base command
        base_command=("curl" "-s" "get.rebar.digital/stable" "|" "bash" "-s" "--" "build-airgap" "--catalog=$catalog")

        # Conditionally add flags based on variables
        [ -n "$location" ] && base_command+=("--location=$location")
        [ -n "$platform" ] && base_command+=("--platform=$platform")
        [ -n "$version" ] && [ -z "$versions" ] && base_command+=("--version=$version")
        [ -n "$versions" ] && base_command+=("--versions=\"$versions\"")
        [ -n "$bootenvs" ] && base_command+=("--download-iso-bootenvs=$bootenvs")
        [ -n "$profiles" ] && base_command+=("--download-iso-profiles=$profiles")

        job_info "base command done: ${base_command[@]}"
      }

      execute_command() {
        # Execute the final command
        job_info "executing command ${base_command[@]}"
        eval "${base_command[@]}"
      }

      move_script_to_final_location() {
        # Move file to final location
        file_to_move="$location/airgap-install.sh"
        {{ $upload_info := ( .ParamExpand "dr-server/airgap-file-upload-location-info" ) }}
        upload_location='{{ get $upload_info "location-type" }}'
        case "$upload_location" in
          drp-server)
            upload_path='{{ get $upload_info "path" }}'
            if [[ -n "$upload_path" ]]
            then
              drpcli files upload $file_to_move as $upload_path
              job_info "file has been moved to $upload_path on the server"
              final_location="{{ .ProvisionerURL }}/files/$upload_path"
            else
              job_error "A `path` was not provided in `dr-server/airgap-file-upload-location-info`"
            fi
            ;;
          s3)
            # Get the s3 url
            # AWS creds? idk figure it out..
            job_error "Storing in s3 has not been implemented yet!!"
            ;;
          *)
            job_error "Location: $upload_location is not supported"
            ;;
        esac
      }

      create_alert() {
        cat << EOF > alert.yaml
      Name: "Airgap bundle is ready!!"
      Level: "$LEVEL"
      Params:
        alert/machine: "$NAME"
        alert/content: "Airgap bundle is ready!"
        alert/level: "$LEVEL"
      Contents: |
        Hello!

        The airgap bundle is ready for use!
        It can be found at $final_location.
        Here are the parameters used to build this bundle:

        platform = $platform
        bootenvs = $bootenvs
        profiles = $profiles
        versions = $versions
        version = $version
        catalog = $catalog
      EOF
        ALERT_ID=$(drpcli alerts create --unique alert.yaml | jq -r '.Uuid')
        job_info ">>> More complete details can be found in Alert ID: 'ux://alerts/$ALERT_ID'"
      }

      # Main script

      job_info "Beginning airgap file upload"

      platform="{{ .Param "dr-server/airgap-platform" }}"
      bootenvs="{{ .Param "dr-server/airgap-iso-bootenvs" }}"
      profiles="{{ .Param "dr-server/airgap-iso-profiles" }}"
      versions="{{ .Param "dr-server/airgap-versions" }}"
      version="{{ .Param "dr-server/initial-version" }}"
      catalog="{{ .Param "dr-server/airgap-catalog" }}"
      location="/tmp/airgap"
      NAME="{{ .Machine.Name }}"
      LEVEL="INFO"

      construct_base_command
      job_info "command constructed"

      execute_command
      job_info "command executed"

      move_script_to_final_location
      job_info "script moved to $upload_location"

      # Set global param for airgap url
      job_info "setting global param to $final_location"
      drpcli profiles set global param dr-server/airgap-file-url to - >/dev/null <<< "$final_location"

      create_alert
      job_info "alert created"

      job_success "Done"
      exit 0

Meta:
  type: "install"
  icon: "heart"
  color: "blue"
  title: "RackN Content"
  feature-flags: "sane-exit-codes"
