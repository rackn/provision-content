#!/usr/bin/env bash

###
#  Evenly divide a total number of integers into as closely equal
#  size groups up to a max size per group.  A remaining left over
#  is allowed and unequal group sizes are allowed.
#  INPUT:   ARGv1 = total count of items
#           ARGv2 = number of groups
#           ARGv3 = maximum possible size of each group
#  OUTPUT:  Prints to stdout a string of int numbers equal to
#           ARGv2 (groups) plus one (remaining unused)
#  EXAMPLE: evenly_group 21 4 5
#           outputs 5 5 5 5 1
#           four groups of 5, with a left over of 1
#
#           evenly_group 19 3 7
#           outputs 7 6 6 0
#           one group of 7, two groups of 6, no left overs
#
#  See the 'evenly-group-test' Stage and Task for examples of using this.
# 
#  Primarily intended for VMware vSphere VSAN disk claiming for more than
#  7 disks on a system.  Disk sets must be grouped in to a total of no
#  more than 5 groups, 1 Cache disk, and up to 7 Capacity disks.
###
evenly_group() {
  local _tot=$1
  local _grps=$2
  local _max=$3
  local _results=()
  local _left=0 _grps _size _rem _count _iter

  _size=$(( _tot / _grps ))
  _rem=$(( _tot % _grps ))

  for (( _iter = 0; _iter < _grps; _iter++ ))
  do
    [[ $_iter -lt $_rem ]] && _count=$(( _size + 1 )) || _count=$_size

    if [[ $_count -gt $_max ]]
    then
      _left=$((_left + _count - _max))
      _count=$_max
    fi

    _results+=($_count)
  done

  [[ $_left -gt 0 ]] && _results+=($_left) || _results+=("0")

  echo ${_results[@]}
}
