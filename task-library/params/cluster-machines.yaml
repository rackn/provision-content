---
Name: "cluster/machines"
Description: "Machines in this cluster"
Documentation: |
  For v4.8 Cluster Pattern, this defines a map of machine types and
  their names.  Each type can have additional parameters.

  The purpose of this map is to uniquely identify the machines
  and allows the machines to be rotated in a controlled way.


  If omitted the following items will be set via the designated Param:

    * `pipeline`: sets the initial pipeline or assigns from `broker/set-pipeline` [has safe default]
    * `workflow`: set the initial workflow or assigns from `broker/set-workflow` [has safe default]
    * `icon`: sets the initial Meta.icon or assigns from `broker/set-icon` [has safe default]
    * `color`: sets the initial Meta.color or assigns from `broker/set-color` [has safe default]
    * `tags`: [] but automatically includes cluster/profile
    * `meta`: meta data to set on the machine by `cluster-add-params`,  if Meta.icon or Meta.color is set, it will over-write the icon/color.
    * `Params`: (note capitalization!): parameters to use in Terraform templates.  Also set on machine during `cluster-add-params`
    * `Profiles`: (note capitalization!): profiles to add to machine.  Set on machine during `cluster-add-params`

  While most of these values are used only during `terraform-apply` when building machines,
  `meta`, `Params`, and `Profiles` are used during `cluster-add-params` to update Meta and Params of the
  provisioned machines.

  Example:

    ```yaml
    machine:
      names:
        - cl_awesome_1
        - cl_awesome_2
        - cl_awesome_3
      Params:
        broker/set-workflow: universal-application-application-base
        broker/set-pipeline: universal-start
        broker/set-icon: server
        broker/set-color: black
      Profiles:
        - my-cluster-profile
    ```

Meta:
  color: "blue"
  icon: "object ungroup"
  title: "RackN Content"
Schema:
  type: "object"
  additionalProperties:
    names:
      required: true
      type: array
      items:
        type: string
    Params:
      type: object
    Profiles:
      type: array
      items:
        type: string
    tags:
      type: array
      items:
        type: string
    icon:
      type: string
    color:
      type: string
      enum:
        - "red"
        - "orange"
        - "yellow"
        - "olive"
        - "green"
        - "teal"
        - "blue"
        - "violet"
        - "purple"
        - "pink"
        - "brown"
        - "grey"
        - "black"
    meta:
      type: object
    workflow:
      type: string
    pipeline:
      type: string
