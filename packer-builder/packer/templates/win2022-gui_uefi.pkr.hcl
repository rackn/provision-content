packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
    virtualbox = {
      source  = "github.com/hashicorp/virtualbox"
      version = "~> 1"
    }
    vmware = {
      source  = "github.com/hashicorp/vmware"
      version = "~> 1"
    }
  }
}

variable "headless" {
  type    = string
  default = "true"
}

variable "complete_directory" {
  type    = string
  default = "complete"
}

variable "boot_wait" {
  type    = string
  default = "5s"
}

variable "output_directory" {
  type    = string
  default = "output"
}

variable "disk_size" {
  type    = string
  default = "40960"
}

variable "iso_checksum" {
  type    = string
  default = "4f1457c4fe14ce48c9b2324924f33ca4f0470475e6da851b39ccbf98f44e7852"
}

variable "iso_url" {
  type    = string
  default = "https://software-download.microsoft.com/download/sg/20348.169.210806-2348.fe_release_svc_refresh_SERVER_EVAL_x64FRE_en-us.iso"
}

variable "memsize" {
  type    = string
  default = "4096"
}

variable "numvcpus" {
  type    = string
  default = "2"
}

variable "virtio_iso_path" {
  type    = string
  default = "virtio-win-0.1.229.iso"
}

variable "vm_name" {
  type    = string
  default = "Win2022_20324"
}

variable "winrm_password" {
  type    = string
  default = "packer"
}

variable "winrm_username" {
  type    = string
  default = "Administrator"
}

variable "ovmf_code" {
  description = "Path of OVMF code file"

  type    = string
  default = "/usr/share/OVMF/OVMF_CODE.fd"
}

variable "ovmf_vars" {
  description = "Path of OVMF variables file"

  type    = string
  default = "/usr/share/OVMF/OVMF_VARS.fd"
  }

variable "boot_command" {
  type    = list(string)
  default = ["<wait10>","<wait5>", "reset<enter>", "<wait5>", "<spacebar>"]
}

source "qemu" "win2022" {
  accelerator        = "kvm"
  boot_command       = var.boot_command
  boot_wait          = var.boot_wait
  communicator       = "winrm"
  cpus               = var.numvcpus
  disk_detect_zeroes = "unmap"
  disk_discard       = "unmap"
  disk_interface     = "virtio-scsi"
  disk_size          = "${var.disk_size}"
  efi_boot           = true
  efi_firmware_code  = var.ovmf_code
  efi_firmware_vars  = var.ovmf_vars
  floppy_files       = ["scripts_2022/uefi/gui/autounattend.xml"]
  format             = "raw"
  headless           = var.headless
  iso_checksum       = "${var.iso_checksum}"
  iso_url            = "${var.iso_url}"
  output_directory   = "${var.output_directory}"
  machine_type       = "q35"
  memory             = "${var.memsize}"
  qemuargs           = [["-cdrom", "${var.virtio_iso_path}"]]
  shutdown_command   = "shutdown /s /t 5 /f /d p:4:1 /c \"Packer Shutdown\""
  shutdown_timeout   = "30m"
  vm_name            = "${var.vm_name}.raw"
  winrm_insecure     = true
  winrm_password     = "${var.winrm_password}"
  winrm_timeout      = "4h"
  winrm_use_ssl      = true
  winrm_username     = "${var.winrm_username}"
}

source "virtualbox-iso" "win2022" {
  boot_command         = ["<spacebar>"]
  boot_wait            = "${var.boot_wait}"
  communicator         = "winrm"
  disk_size            = "${var.disk_size}"
  format               = "ova"
  // floppy_files         = ["scripts_2022/uefi/gui/autounattend.xml"]
  guest_additions_mode = "disable"
  guest_os_type        = "Windows2019_64"
  headless             = var.headless
  iso_checksum         = "${var.iso_checksum}"
  iso_interface        = "sata"
  iso_url              = "${var.iso_url}"
  output_directory     = "${var.output_directory}"
  shutdown_command     = "shutdown /s /t 5 /f /d p:4:1 /c \"Packer Shutdown\""
  shutdown_timeout     = "30m"
  vboxmanage           = [
    ["modifyvm", "{{ .Name }}", "--memory", "${var.memsize}"],
    ["modifyvm", "{{ .Name }}", "--cpus", "${var.numvcpus}"],
    ["modifyvm", "{{ .Name }}", "--firmware", "EFI"],
    ["modifyvm", "{{ .Name }}", "--boot1", "dvd"],
    ["modifyvm", "{{ .Name }}", "--boot2", "floppy"],
    ["storageattach", "{{ .Name }}", "--storagectl", "SATA Controller", "--type", "dvddrive", "--port", "3", "--medium", "./scripts_2022/uefi/gui/autounattend.iso"]
    ]
  vm_name              = "${var.vm_name}"
  winrm_insecure       = true
  winrm_password       = "${var.winrm_password}"
  winrm_timeout        = "4h"
  winrm_use_ssl        = true
  winrm_username       = "${var.winrm_username}"
}

source "vmware-iso" "win2022" {
  boot_command     = ["<spacebar>"]
  boot_wait        = "${var.boot_wait}"
  communicator     = "winrm"
  disk_size        = "${var.disk_size}"
  disk_type_id     = "0"
  floppy_files     = ["scripts_2022/uefi/gui/autounattend.xml"]
  guest_os_type    = "windows2019srv-64"
  headless         = var.headless
  iso_checksum     = "${var.iso_checksum}"
  iso_url          = "${var.iso_url}"
  output_directory = "${var.output_directory}"
  shutdown_command = "shutdown /s /t 5 /f /d p:4:1 /c \"Packer Shutdown\""
  shutdown_timeout = "30m"
  skip_compaction  = false
  vm_name          = "${var.vm_name}"
  vmx_data = {
    firmware            = "efi"
    memsize             = "${var.memsize}"
    numvcpus            = "${var.numvcpus}"
    "scsi0.virtualDev"  = "lsisas1068"
    "virtualHW.version" = "14"
  }
  winrm_insecure = true
  winrm_password = "${var.winrm_password}"
  winrm_timeout  = "4h"
  winrm_use_ssl  = true
  winrm_username = "${var.winrm_username}"
}

build {
  sources = ["source.qemu.win2022", "source.virtualbox-iso.win2022", "source.vmware-iso.win2022"]

  provisioner "powershell" {
    only         = ["vmware-iso"]
    pause_before = "1m0s"
    scripts      = ["scripts_2022/vmware-tools.ps1"]
  }

  provisioner "powershell" {
    only         = ["virtualbox-iso"]
    pause_before = "1m0s"
    scripts      = ["scripts_2022/virtualbox-guest-additions.ps1"]
  }

  provisioner "powershell" {
    scripts = ["scripts_2022/setup.ps1"]
  }

  provisioner "windows-restart" {
    restart_timeout = "30m"
  }

  provisioner "powershell" {
    scripts = ["scripts_2022/win-update.ps1"]
  }

  provisioner "windows-restart" {
    restart_timeout = "30m"
  }

  provisioner "powershell" {
    scripts = ["scripts_2022/win-update.ps1"]
  }

  provisioner "windows-restart" {
    restart_timeout = "30m"
  }

  provisioner "powershell" {
    script = "scripts/provision-openssh.ps1"
  }

  provisioner "powershell" {
    script = "scripts/install-chocolatey.ps1"
  }

  provisioner "windows-restart" {
  }

  provisioner "powershell" {
    script = "scripts/install-choco-packages.ps1"
  }

  provisioner "windows-restart" {
  }

  provisioner "powershell" {
    script = "scripts/provision-cloudbase-init.ps1"
  }

  provisioner "powershell" {
    script = "scripts/rackn-setup.ps1"
  }

  provisioner "powershell" {
    pause_before = "1m0s"
    scripts      = ["scripts_2022/cleanup.ps1"]
  }
}
