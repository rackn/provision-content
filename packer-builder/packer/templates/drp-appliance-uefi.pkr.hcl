packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
    hyperv = {
      version = ">= 1.1.3"
      source  = "github.com/hashicorp/hyperv"
    }
    virtualbox = {
      source  = "github.com/hashicorp/virtualbox"
      version = "~> 1"
    }
  }
}

variable "headless" {
  type    = string
  default = "true"
}

variable "ansible_override" {
  type    = string
  default = ""
}

variable "boot_command" {
  type    = string
  default = ""
}

variable "disk_size" {
  type    = string
  default = "70000"
}

variable "disk_additional_size" {
  type    = list(number)
  default = ["1024"]
}

variable "memory" {
  type    = string
  default = "1024"
}

variable "cpus" {
  type    = string
  default = "1"
}

variable "iso_checksum" {
  type    = string
  default = "4a8c4ed4b79edd0977d7f88be7c07e12c4b748671a7786eb636c6700e58068d5"
}

variable "iso_url" {
  type    = string
  default = "https://repo.almalinux.org/almalinux/9/isos/x86_64/AlmaLinux-9.3-x86_64-dvd.iso"
}

variable "complete_directory" {
  type    = string
  default = "complete"
}

variable "output_directory" {
  type    = string
  default = "output"
}
variable "provision_script_options" {
  type    = string
  default = ""
}
variable "output_vagrant" {
  type    = string
  default = ""
}
variable "ssh_password" {
  type      = string
  default   = ""
  sensitive = true
}
variable "ssh_username" {
  type    = string
  default = "root"
}

variable "switch_name" {
  type    = string
  default = ""
}

variable "vagrantfile_template" {
  type    = string
  default = ""
}

variable "vlan_id" {
  type    = string
  default = ""
}

variable "vm_name" {
  type    = string
  default = ""
}

variable "ansible_variables" {
  type    = string
  default = ""
}

variable "ansible_playbook" {
  type    = string
  default = ""
}

variable "uefi_file" {
  type    = string
  default = ""
}

variable "boot_command_linux_uefi" {
  type    = list(string)
  default = ["c <wait> setparams 'kickstart' <enter>",
    " linuxefi /images/pxeboot/vmlinuz",
    " inst.stage2=hd:LABEL=AlmaLinux-9-3-x86_64-dvd",
    " inst.ks=http://{{ .HTTPIP }}:{{ .HTTPPort }}/almalinux/9/kvm/ks.cfg<enter>",
    " initrdefi /images/pxeboot/initrd.img<enter> boot<enter>"]
}

variable "neofetch_file" {
  type    = string
  default = ""
}

variable "root_shutdown_command" {
  description = "The command to use to gracefully shut down the machine"

  type    = string
  default = "/sbin/shutdown -hP now"
}

// OVMF files are in different paths depending on the distribution
// ubuntu 22.04:
// root@pm3:~# cd /usr/share/OVMF/
// root@pm3:/usr/share/OVMF# ls
// OVMF_CODE_4M.fd     OVMF_CODE_4M.secboot.fd   OVMF_CODE.fd     OVMF_CODE.secboot.fd  OVMF_VARS_4M.ms.fd        OVMF_VARS.fd
// OVMF_CODE_4M.ms.fd  OVMF_CODE_4M.snakeoil.fd  OVMF_CODE.ms.fd  OVMF_VARS_4M.fd       OVMF_VARS_4M.snakeoil.fd  OVMF_VARS.ms.fd

// Rocky 8.9
// [root@immich ~]#  cd /usr/share/OVMF/
// [root@immich OVMF]# ls
// OVMF_CODE.secboot.fd  OVMF_VARS.fd  OVMF_VARS.secboot.fd  UefiShell.iso

variable "ovmf_code" {
  description = "Path of OVMF code file"

  type    = string
  default = "/usr/share/OVMF/OVMF_CODE.fd"
}

variable "ovmf_vars" {
  description = "Path of OVMF variables file"

  type    = string
  default = "/usr/share/OVMF/OVMF_VARS.fd"
}


source "hyperv-iso" "vm" {
  boot_command          = ["${var.boot_command}"]
  boot_wait             = "5s"
  communicator          = "ssh"
  cpus                  = "${var.cpus}"
  disk_block_size       = "1"
  disk_size             = "${var.disk_size}"
  enable_dynamic_memory = "true"
  enable_secure_boot    = false
  generation            = 2
  guest_additions_mode  = "disable"
  http_directory        = "./extra/files"
  iso_checksum          = "${var.iso_checksum_type}:${var.iso_checksum}"
  iso_url               = "${var.iso_url}"
  memory                = "${var.memory}"
  output_directory      = "${var.output_directory}"
  shutdown_command      = "echo 'rackndeploy' | sudo -S shutdown -P now"
  shutdown_timeout      = "4h"
  ssh_password          = "${var.ssh_password}"
  ssh_timeout           = "4h"
  ssh_username          = var.ssh_username
  temp_path             = "."
  vlan_id               = "${var.vlan_id}"
  vm_name               = "${var.vm_name}"
  switch_config {
    switch_name = "${var.switch_name}"
  }
  switch_config {
    switch_name = "DRP-Internal"
  }
}

// heavily borrowed from https://github.com/AlmaLinux/cloud-images/tree/master
source "qemu" "vm" {
  iso_url            = var.iso_url
  iso_checksum       = var.iso_checksum
  shutdown_command   = var.root_shutdown_command
  http_directory     = "./extra/files"
  ssh_username       = var.ssh_username
  ssh_password       = var.ssh_password
  ssh_timeout        = "10m"
  output_directory   = var.output_directory
  efi_firmware_code  = var.ovmf_code
  efi_firmware_vars  = var.ovmf_vars
  efi_drop_efivars   = true
  cpus               = var.cpus
  disk_interface     = "virtio-scsi"
  disk_size          = var.disk_size
  disk_cache         = "unsafe"
  disk_discard       = "unmap"
  disk_detect_zeroes = "unmap"
  disk_compression   = true
  format             = "raw"
  headless           = var.headless
  memory             = var.memory
  net_device         = "virtio-net"
  vm_name            = "${var.vm_name}-uefi.x86_64"
  boot_wait          = "10s"
  boot_command       = var.boot_command_linux_uefi
  machine_type       = "q35"
  accelerator        = "kvm"
  qemuargs = [
    ["-cpu", "host"]
  ]
}

source "virtualbox-iso" "vm" {
  boot_command         = var.boot_command_linux_uefi
  boot_wait            = "10s"
  cpus                 = var.cpus
  disk_size            = var.disk_size
  firmware             = "efi"
  format               = "ova"
  guest_additions_mode = "disable"
  guest_os_type        = "RedHat_64"
  hard_drive_interface = "sata"
  iso_interface        = "sata"
  headless             = var.headless
  http_directory       = "./extra/files"
  iso_checksum         = var.iso_checksum
  iso_url              = var.iso_url
  memory               = var.memory
  output_directory     = var.output_directory
  shutdown_command     = var.root_shutdown_command
  ssh_password         = var.ssh_password
  ssh_timeout          = "10m"
  ssh_username         = var.ssh_username
  vm_name              = "${var.vm_name}-uefi.x86_64"
  vboxmanage = [
    ["modifyvm", "{{.Name}}", "--nat-localhostreachable1", "on"],
    ["modifyvm", "{{ .Name }}", "--memory", "${var.memory}"],
    ["modifyvm", "{{ .Name }}", "--cpus", "${var.cpus}"],
    ["modifyvm", "{{ .Name }}", "--firmware", "EFI"],
  ]
}

build {
  sources = ["source.hyperv-iso.vm", "source.qemu.vm", "source.virtualbox-iso.vm"]

  provisioner "file" {
    destination = "/tmp/build-appliance.sh"
    source      = "scripts/build-appliance.sh"
  }

  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; {{ .Vars }} sudo -E sh '{{ .Path }}'"
    inline          = ["chmod +x /tmp/build-appliance.sh", "/tmp/build-appliance.sh 'bootstrap-appliance-hyper-v,'"]
    inline_shebang  = "/bin/sh -x"
  }

  provisioner "file" {
    destination = "/usr/local/bin/uefi.sh"
    source      = "${var.uefi_file}"
  }

  provisioner "file" {
    destination = "/tmp/zeroing.sh"
    source      = "extra/files/gen2-linux/zeroing.sh"
  }

  provisioner "shell" {
    execute_command = "chmod +x {{ .Path }}; {{ .Vars }} sudo -E sh '{{ .Path }}'"
    inline          = ["echo Last Phase", "chmod +x /usr/local/bin/uefi.sh", "systemctl set-default multi-user.target", "/bin/rm -f /etc/ssh/*key*", "chmod +x /tmp/zeroing.sh", "/tmp/zeroing.sh", "/bin/rm -rfv /tmp/*"]
    inline_shebang  = "/bin/sh -x"
    pause_before    = "30s"
  }
}
