#!/usr/bin/env bash

{{ template "setup.tmpl" . }}

# quick and dirty setup of VirtualBox to be used by packer

set -e
function setup_ubuntu_22() {
  install_lookup software-properties-common
  wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
  echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
  apt update
  install_lookup virtualbox-7.0
}

function setup_rhelish_virtualbox() {
  dnf install epel-release -y
  dnf install wget curl gcc make perl bzip2 dkms kernel-devel kernel-headers  -y

  dev_kernal_version=$( rpm -q kernel-devel )
  kernel_version=$( uname -r )
  if [ "$dev_kernal_version" != "kernel-devel-${kernel_version}" ]; then
    echo "Kernel version mismatch, rebooting to load the correct kernel"
    dnf update -y
    reboot
  fi
  dnf config-manager --add-repo=https://download.virtualbox.org/virtualbox/rpm/el/virtualbox.repo
  dnf install VirtualBox-7.0 -y

  /sbin/vboxconfig
}

function setup_rhelish8() {
  setup_rhelish_virtualbox
}

function setup_rhelish9() {
  setup_rhelish_virtualbox
}

main() {

  case $osfamily in
  ubuntu)
    case $osversion in
      22.04)
        setup_ubuntu_22
        ;;
      *)
        xiterr 1 "Unsupported Ubuntu version '$osversion'."
        ;;
    esac
    ;;
  almalinux|centos|redhat|rhel|fedora|rocky)
    case $osversion in
      8.*)
        setup_rhelish8
        ;;
      9.*)
        setup_rhelish9
        ;;
      *)
        xiterr 1 "Unsupported RHEL version '$osversion'."
        ;;
    esac
    ;;
  *)
    xiterr 1 "Unsupported OS family '$osfamily'."
    ;;
  esac

  vboxmanage --version
  echo "VirtualBox setup complete."
}

main $*
exit $?