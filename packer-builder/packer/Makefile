# Disable builtin rules and variables since they aren't used
# This makes the output of "make -d" much easier to follow and speeds up evaluation
MAKEFLAGS+= --no-builtin-rules
MAKEFLAGS+= --no-builtin-variables

# Normal (libvirt and VirtualBox) images
#IMAGES+= windows-2012-r2
#IMAGES+= windows-2016
# IMAGES+= windows-2019
#IMAGES+= windows-2019-uefi
#IMAGES+= windows-10
#IMAGES+= windows-10-1903
#IMAGES+= windows-10-1909
# linux
#IMAGES+= linux-ubuntu-1804
#IMAGES+= ubuntu-1804
IMAGES+= win-2022-gui-uefi
IMAGES+= win-2019-gui-uefi
IMAGES+= drp-appliance-v1-uefi


# Images supporting vSphere
#VSPHERE_IMAGES+= windows-2016
# VSPHERE_IMAGES+= windows-2019
#VSPHERE_IMAGES+= windows-10

SETUP:=$(shell bash scripts/make-setup.sh)

# Generate build-* targets
#LIN_VIRTBOX_BUILDS= $(addsuffix -virtualbox,$(addprefix linux/$(LIN_IMAGES)/build-,$(LIN_IMAGES)))
#LIN_LIBVIRT_BUILDS= $(addsuffix -libvirt,$(addprefix linux/$(LIN_IMAGES)/build-,$(LIN_IMAGES)))
#LIN_VSPHERE_BUILDS= $(addsuffix -vsphere,$(addprefix linux/$(LIN_IMAGES)/build-,$(LIN_IMAGES)))

#LIN_LIBVIRT_BUILDS= $(addsuffix -libvirt,$(addprefix linux-build-,$(LIN_IMAGES)))
# LIN_LIBVIRT_BUILDS= $(addsuffix -libvirt,$(addprefix build-linux,$(LIN_IMAGES)))

VIRTBOX_BUILDS= $(addsuffix -virtualbox,$(addprefix build-,$(IMAGES)))
LIBVIRT_BUILDS= $(addsuffix -kvm,$(addprefix build-,$(IMAGES)))
VSPHERE_BUILDS= $(addsuffix -vsphere,$(addprefix build-,$(VSPHERE_IMAGES)))

.PHONY: help $(VIRTBOX_BUILDS) $(LIBVIRT_BUILDS) $(VSPHERE_BUILDS)

help:
	@echo Type one of the following commands to build a specific windows box.
	@echo
	@echo VirtualBox Targets:
	@$(addprefix echo make ,$(addsuffix ;,$(VIRTBOX_BUILDS)))
	@echo
	@echo libvirt Targets:
	@$(addprefix echo make ,$(addsuffix ;,$(LIBVIRT_BUILDS)))
	@echo
	@echo vSphere Targets:
	@$(addprefix echo make ,$(addsuffix ;,$(VSPHERE_BUILDS)))

# Target specific pattern rules for build-* targets
#$(LIN_VIRTBOX_BUILDS): linux/$(LIN_IMAGES)/build-%-virtualbox: %-amd64-virtualbox.box
#$(LIN_VIRTBOX_BUILDS): $(LIN_IMAGES)/build-%-virtualbox: %-amd64-virtualbox.box

#SYG
#$(LIN_LIBVIRT_BUILDS): linux/$(LIN_IMAGES)/build-%-libvirt: %-amd64-libvirt.box
#$(LIN_LIBVIRT_BUILDS): linux-build-%-libvirt: %-amd64-libvirt.box
#$(LIN_LIBVIRT_BUILDS): build-%-libvirt: %-amd64-libvirt.box

#$(LIN_VSPHERE_BUILDS): linux/$(LIN_IMAGES)/build-%-vsphere: %-amd64-vsphere.box
#$(LIN_VSPHERE_BUILDS): $(LIN_IMAGES)/build-%-vsphere: %-amd64-vsphere.box

# $(VIRTBOX_BUILDS): build-%-virtualbox: %-amd64-virtualbox.box
# $(LIBVIRT_BUILDS): build-%-kvm: %-amd64-libvirt.box
# $(VSPHERE_BUILDS): build-%-vsphere: %-amd64-vsphere.box


virtio-win-0.1.229.iso:
	wget https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/virtio-win-0.1.229-1/virtio-win-0.1.229.iso

build-win-2022-gui-uefi-kvm: virtio-win-0.1.229.iso
	rm -rf output complete manifest.json
	template=templates/win2022-gui_uefi.pkr.hcl  && \
		packer init -upgrade $$template && \
		packer build -only=qemu.* -var-file=variables/win2022-gui_uefi.hcl $$template

build-win-2022-gui-uefi-virtualbox: virtio-win-0.1.229.iso
	rm -rf output complete manifest.json
	template=templates/win2022-gui_uefi.pkr.hcl && \
		packer init -upgrade $$template && \
		packer build -only=virtualbox-iso.* -var-file=variables/drp-appliance.pkvars.hcl $$template


build-win-2019-gui-uefi-kvm: virtio-win-0.1.229.iso
	rm -rf output complete manifest.json
	template=templates/win2019-gui_uefi.pkr.hcl  && \
		packer init -upgrade $$template && \
		packer build -only=qemu.* -var-file=variables/win2019-gui_uefi.hcl $$template

build-win-2019-gui-uefi-virtualbox: virtio-win-0.1.229.iso
	rm -rf output complete manifest.json
	template=templates/win2019-gui_uefi.pkr.hcl && \
		packer init -upgrade $$template && \
		packer build -only=virtualbox-iso.* -var-file=variables/win2019-gui_uefi.hcl $$template


build-drp-appliance-v1-uefi-kvm: templates/drp-appliance-uefi.pkr.hcl
	rm -rf output complete manifest.json
	packer init -upgrade templates/drp-appliance-uefi.pkr.hcl
	packer build -only=qemu.* -var-file=variables/drp-appliance.pkvars.hcl  templates/drp-appliance-uefi.pkr.hcl

build-drp-appliance-v1-uefi-virtualbox: templates/drp-appliance-uefi.pkr.hcl
	rm -rf output complete manifest.json
	packer init -upgrade templates/drp-appliance-uefi.pkr.hcl
	packer build -only=virtualbox-iso.* -var-file=variables/drp-appliance.pkvars.hcl  templates/drp-appliance-uefi.pkr.hcl
