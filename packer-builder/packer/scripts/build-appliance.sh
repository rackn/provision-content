#!/usr/bin/env bash

set -x
echo "Installing dr-provision: extra params: ${1}"
sleep 30

echo "Get initial tools"
sudo dnf -y install wget curl net-tools cloud-init

# make image-deploy happy
mkdir /curtin

# match all interfaces, set to dhcp
echo > /etc/netplan/00-installer-config.yaml <<EOF
network:
  version: 2
  ethernets:
    alleths:
      match:
        name: en*
      dhcp4: true
EOF

echo "Get initial appliance items"
wget https://rebar-catalog.s3.us-west-2.amazonaws.com/box2/appliance.yaml
wget https://rebar-catalog.s3.us-west-2.amazonaws.com/box2/talos.yaml
wget https://rebar-catalog.s3.us-west-2.amazonaws.com/box2/greg-license.yaml
cp appliance.yaml /tmp/appliance.yaml
cp talos.yaml /tmp/talos.yaml
cp greg-license.yaml /tmp/rackn-license.yaml

echo "Get install.sh and run it"
curl -fsSL get.rebar.digital/tip > /tmp/install.sh
sudo chmod +x /tmp/install.sh
sudo /tmp/install.sh install --universal --drp-id=freddie-1 \
  --initial-contents=/tmp/rackn-license.yaml,/tmp/appliance.yaml,/tmp/talos.yaml \
  --initial-profiles=bootstrap-appliance,bootstrap-alma-9-min,bootstrap-talos,${1}bootstrap-appliance-complete

echo 'should wait for drp to finish'
sleep 60

if [ ! -f /usr/local/bin/drpcli ] ; then
    echo "missing drpcli"
    exit 1
fi

await="$(/usr/local/bin/drpcli machines await Name:freddie-1 "Or(Runnable=Eq(false),WorkflowComplete=Eq(true))" --timeout 2400)"
echo "Await returned $await"
if [[ "$await" != "complete" ]] ; then
    echo "await: $await"
    exit 1
fi

sleep 20

runnable="$(/usr/local/bin/drpcli machines show Name:freddie-1 -J "Runnable" -F text -H | head -1 | awk '{ print $2 }')"
stacks="$(/usr/local/bin/drpcli machines show Name:freddie-1 -J "TaskErrorStacks" -F text -H | head -1 | awk '{ print $2 }')"
echo "Checking to see if it failed: $runnable $stacks"
if [ "$runnable" == "false" ] | [ "$stacks" != "[]" ] ; then
    echo "failed!!!"
    sleep 360
    exit 1
fi

echo "Cleaning up files"
sudo rm -f /tmp/*.yaml

echo "Rename the server"
/usr/local/bin/drpcli machines update Name:freddie-1 '{ "Locked": false }' > /dev/null || true
/usr/local/bin/drpcli machines update Name:freddie-1 '{ "Name": "drp-trial", "Locked": true }' >/dev/null || true

echo "Stopping DRP"
sudo systemctl daemon-reload
sudo systemctl stop dr-provision

echo "Clear podman"
sudo podman rm $(sudo podman ps -aq) -f

echo "Clear root"
sudo rm -rf /root/*

echo "Clear tmp"
sudo rm -rf /tmp/*

exit 0
