# Packer Builder Content

The content in this directory provides a Digital Rebar content pack to
setup for, and perform Packer image builds.  These images are uploaded to
the DRP Endpoint, and ready to be used in the RackN `image-deploy`
plugin.

This content provides complete setup of packer, KVM, and virtualbox.
The intent of this project is to provide workflows that enable creating
a packer build machine, running curated packer builds and uploading
the resulting images to the DRP endpoint.

This content runs on Ubuntu 22.04, and RHEL-ish linuxes versions 8 and 9.
*Ubuntu 22.04* is the most tested path. 

This directory contains two primary pieces:

  * `content` - the content pack that creates the workflows, params, tasks (etc.) to drive the Packer build automation
  * `packer` - Packer JSON definitions for supported Operating Systems, and scripts for customization of the OS images

If this content pack (``packer-builder``) is not yet available in the *Catalog* you
will need to ``git checkout ...`` the source, and bundle/upload it to your DRP
Endpoint.  See the directions below.

Currently the Packer pieces are not templatized nor very flexible.

To customize image builds today; you will need to ``git clone`` the ``packer``
directory, customize the JSON and scripts, then use the Params to customize the
Git repo (and potentially) Branch to checkout to build your gold master images
against.

## Install the Digital Rebar Content Pack

The ``content`` directory is a Digital Rebar content pack.  If you do not
have the content (``packer-builder``) available in your catalog, you can bundle
 and install this with the following steps:

    mkdir -p digitalrebar && cd digitalrebar
    git init
    git remote add origin https://gitlab.com/rackn/provision-content.git
    git fetch origin
    git checkout origin/v4 -- packer-builder
    cd provision-content/packer-builder/content
    ### the dir 'provision-content/packer-builder/packer' contains the Packer build definition info
    drpcli contents bundle ../packer-builder.yaml Version="v20240402-000000"
    drpcli contents upload ../packer-builder.yaml

***Note***: The ``packer`` directory can be used when running Packer manually, but you
          will be left with the task of setting up the build environment and hypervisor versions
          that match the Packer requirements.

## Using this Content Pack

Once you have the content pack bundled and uploaded, and if appropriate, the JSON
structures customized and available for checkout by the Workflow, perform the
following to use the content.

***Warning***: It is HIGHLY recommended you do NOT modify the packer configuration for the
             builds your first time through.  Packer and QEMU build arguments
             are EXTREMELY FRAGILE.  Until you have a tested image build, we do not
             recommend customizing things.

  1. Check out the github repo, bundle and upload the content pack in the ``content`` directory
  2. Have a single Bare Metal machine available and dedicated to your gold master builds. If a VM is your only option, ensure nested virtualization is enabled, and most things will work.
  3. Discover and perform a fresh Ubuntu 22.04 install - RHEL-ish linuxes versions 8 and 9 are somewhat supported
  4. Ensure you have the DRP Agent in place in the installed Ubuntu build machine
  5. Set the Params appropriately to customize (at minimum) the Image to be built (see below)
  6. Run the ``packer-setup-and-build`` workflow

Wait from a few minutes to several hours. The build time is highly dependent on the
build picked.  Expect between 3 to 6 hours at a minimum for windows builds.

At the end of the process, the Build artifacts should be built and uploaded to your DRP
Endpoint in the Files space under ``/files/images/builder/``.

Versioning does not exist, so to rebuild an image, you will need to delete the existing image
from DRP in the files section.

## Defining What OS to Build

Curated builds are defined in packer builder that contain the Digital Rebar Agent.
They are minimally customized. The intent is that you'll create your own
packer configuration according to your policies and needs, with these
as a starting point.

See the content Params for more documentation.  At a minimum, you'll need:

* `packer-builder/build-image` - defines what supported Operating System to build. Current options are
  * `build-win-2022-gui-uefi-virtualbox`
  * `build-drp-appliance-v1-uefi-virtualbox`
  * `build-win-2022-gui-uefi-kvm`
  * `build-drp-appliance-v1-uefi-kvm`

Additionally, possibly:

* `packer-builder/var-file` - a small amount of customization can be performed on the Packer configuration by setting this to a JSON structure of variables.json like data - see param for documentation
* `packer-builder/git-branch` - define the Git branch that contains the Packer customizations

Read the Params documentation ... all of them.

### External Builds

Once you start building your own packer configurations, you can use the
following parameters to define the git repo and branch to check out and build.

As long as your build outputs one or more raw image files, the workflow will upload it to the DRP.
Is it recommended that you install DRP into your built images to interoperate with the DRP Endpoint.

* `packer-builder/external-repo-url` - the URL to the git repo containing the Packer configuration
* `packer-builder/external-repo-branch` - the branch to check out from the git repo
* `packer-builder/external-packer-config` - the path to the main Packer hcl file in the git repo
* `packer-builder/external-packer-path` - the path within the git repo to run packer
* `packer-builder/external-packer-var-file` - the path to the Packer var file in the git repo
* `packer-builder/packer-args` - additional arguments to pass to Packer

## Deploying Your Builds

Like any standard RackN / Digital Rebar image-deploy image, simply specify the Params
that describe your deployable image.  For example:

* ``image-deploy/image-file: files/images/builder/windows-2016-amd64-libvirt.img.xz``
* ``image-deploy/image-os: windows``
* ``image-deploy/image-type: dd-xz``

## Logging In To Your Deployed Images

ToDo

Additionally, the Windows images has OpenSSH server service installed.  NOTE that the OpenSSH
service on Windows is extremely fragile.  It'll freeze up on you regularly.  You have been
warned.

## A Note about Hardware Drivers

Your target hardware or systems for deploying your image to, must be supported by the drivers
in the image.  If the stock Operating System installation does not support your hardware,
you will need to modify the Packer ``provisioners`` stanzas to inject appropriate drivers.
The installations will be performed at the command line, in the running image as it is built.
See some of the existing PowerShell scripts in use to make image customizations as inspiration.

## A Note about Disk Space

Your build target platform will need gobs of it.  At a minimum, 100 GB of free space for a
Windows image build.  There are multiple copies of the build artifacts that are created and
used through the process, do not rely on just the "disk size" specified in the Packer JSON.

Typically the disk space will be consumed in the root file system of the build system.  If you
need to relocate the build artifacts and working space to a different directory or file system,
see the ``packer-builder/var-file`` for customization of the ``output`` and ``complete``
directories.
