---
Name: resource-proxmox-cloud
Description: "Manage Proxmox Resources"
Documentation: |
  Manages Proxmox Instances via Resource Brokers - currently only
  supports Proxmox VMs via PXE boot, and not Cloud Image deploy
  (eg deployed from "templates").

  There are multiple scenarios that the Cluster create process needs
  to support with regard to Virtual Machines created on a Proxmox
  cluster.  Depending on the desired behavior for newly created
  Cluster resources (eg *machines*); you will need to specify the
  cluster behaviors appropriately.

  Potentially desired behaviors:

    * Machines PXE boot in to Discovery (sledgehammer), and wait for interactive input
    * Machines PXE boot and are automatically assigned a Pipeline for installation
      (either by setting the Pipeline in the Cluster configuration, or via a Classifier
      that performs the same action)
    * Machines are created from a "*Cloud Image*" similar to a Public Cloud behavior

  It is important to note that the `cluster/wait-filter` is used to
  handle the status management of the synchronization between the DRP
  based Machine object, and the backing resource that is created.

  The PXE boot behaviors are supported with this Resource Broker, but
  each behavior requies a different Filter and/or Workflow setting to
  correctly instantiate resources to a successful state.  This must be
  done on a cluster-by-cluster basis and you can not mix and match
  the behaviors on the same cluster.

  The "Cloud Image", behavior is not supported in the DRP Proxmox
  Cluster capability yet, and as such has no defined Filter
  specifications.  Presumably, the behavior will be supported by
  the default Filter specification, but has not yet been tested.


  Machines that PXE boot and are assigned an OS install Pipeline will
  very often end up with a `Runnable: false` condition.  The default
  defined Filter will cause a Cluster provision failure in this case.

  The `cluster/wait-filter` is required for the synchronization between
  the DRP Machine object, and the backing resource that is created by
  the Resource Broker.


  **PXE boot with Install Pipeline**

  For Machines that are discovered via the standard Sledgehammer discovery
  process, and then transitioned manually by an operator to an OS Install
  require a different Filter.  This Resource Broker sets the Param
  `cluster/wait-filter` to the following:

    * `And(Workflow=Eq(universal-runbook),WorkflowComplete=Eq(true),BootEnv=Eq(local))`

  !!! note
      This assumes that the Pipeline ends with the final chained
      workflow being set to `universal-runbook`.  If the Pipeline
      uses a different Workflow as the final/finishing workflow, this
      Filter needs to be adjusted to reference the last Workflow in
      the chain

  You MUST also set the Pipeline and Workflow Params on the Cluster, and
  example which defines Photon Linux 4 OS install would look like:

    * **EXAMPLE**
    * `broker-set-pipeline: universal-application-photon4`
    * `broker-set-workflow: universal-discover`

  The `universal-discover` workflow is usually the correct starting
  point for the OS install pipelines, as defined by the
  `universal/workflow-chain-map` (or via some other override param value
  being set)

  !!! warning
      If the `cluster/wait-filter` is NOT set as defined above,
      the `cluster-provision` task WILL fail when the machine
      chains in to the OS installer workflow.

  **PXE boot and Discovery Only**

  If the intent is to have Machines created and stop and wait in
  Sledgehammer without zero-touch provisioning / automation transition
  to an OS install Pipeline, you MUST leave the Filter for the Cluster
  at the default value, which is the following:S

    * `Or(Runnable=Eq(false),WorkflowComplete=Eq(true))`

  **AND** you must set `broker/set-pipeline` and `broker/set-workflow`
  Params to:

    * `broker-set-pipeline: universal-application-discover`
    * `broker-set-workflow: universal-discover`

  !!! note
      Normally cluster configuration values should be changed on the
      dynamically created Profile with the same name as the cluster.

  !!! warning
      If the backing Machine object encounters any Workflow failures,
      the cluster provision operation will fail.  In many cases,
      if the `cluster-provision` Task hasn't failed, but the backing
      Machine can be corrected (eg getting stuck in `centos-only-repos`)
      then the `cluster-provision` may succeed.

Meta:
  application: cluster
  platform: proxmox
  major: "1"
  minor: "0"
  required: "proxmox/node,proxmox/user,proxmox/password"
  optional: "proxmox/api-port,proxmox/api-url,proxmox/tls-insecure,proxmox/bridge,proxmox/otp,proxmox/provider-source,proxmox/parallel_create_limit,proxmox/provider-version,proxmox/proxy-server"
  cluster-add-optional: "proxmox/api-port,proxmox/otp,proxmox/provider-source,proxmox/parallel_create_limit,proxmox/provider-version,proxmox/proxy-server,proxmox/bridge"
  BaseContext: "terraform"
  icon: "cloud"
  color: "orange"
  title: "RackN Content"
Params:
  # if the Machine gets a Pipeline beyond "discover", then set the filter to this
  #  cluster/wait-filter: "And(Workflow=Eq(universal-runbook),WorkflowComplete=Eq(true),BootEnv=Eq(local))"
  cloud/join-method: "discovery"
  cloud/ansible-joinup: false
  broker/type: "cloud-terraform"
  broker/set-pipeline: "universal-application-discover"
  broker/set-workflow: "universal-discover"
  broker/set-icon: "boxes"
  broker/set-color: "orange"
  cloud/provider: "proxmox"
  terraform/map-instance-id: "self.id"
  terraform/map-instance-name: "self.name"
  terraform/map-mac-address: "self.network[0].macaddr"
  terraform/map-disk-volume: "self.disk.volume"
  terraform/plan-instance-resource-name: "proxmox_vm_qemu"
  terraform/plan-templates:
    - cloud-provision-proxmox-init.tf.tmpl
    - cloud-provision-machines.tf.tmpl
  terraform/plan-instance-template: cloud-provision-proxmox-instance.tf.tmpl
