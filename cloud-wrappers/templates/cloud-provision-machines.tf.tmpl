{{if .ParamExists "cluster/machine-types" -}}
{{  range $k := .ParamExpand "cluster/machine-types" -}}
{{    $cl := dict -}}
{{    if $.ParamExists "cluster/machines" -}}
{{      $cl = $.ParamExpand "cluster/machines" }}
{{    end -}}
{{    $pmap := dig $k "Params" (dict) $cl -}}
{{    $_ := set $pmap "root" $ -}}
resource "{{$.Param "terraform/plan-instance-resource-name"}}" "drp_{{$k}}" {
  for_each    = toset(var.drp_{{$k}}_names)

  {{ $.CallTemplate ($.Param "terraform/plan-instance-template") $pmap }}

{{    if eq ( $.Param "cloud/join-method" ) "synchronize" -}}

  provisioner "local-exec" {
    command = <<EO_SYNC_CREATE
      ./synchronize.sh \
        --action create \
        --id '\${ {{$.Param "terraform/map-instance-id"}} }' \
        --name '\${ {{$.Param "terraform/map-instance-name"}} }' \
        --address '\${ {{$.Param "terraform/map-ip-address"}} }' \
        --private '\${ {{if $.ParamExists "terraform/map-private-ip-address"}}{{$.Param "terraform/map-private-ip-address"}}{{else}}{{$.Param "terraform/map-ip-address"}}{{end}} }' \
        --workflow '\${var.drp_{{$k}}_workflow}' \
        --pipeline '\${var.drp_{{$k}}_pipeline}' \
        --tags '\${var.drp_{{$k}}_tags}' \
        --icon '\${var.drp_{{$k}}_icon}' \
        --color '\${var.drp_{{$k}}_color}'
EO_SYNC_CREATE
  }

  provisioner "local-exec" {
    command = <<EO_SYNC_DESTROY
      ./synchronize.sh \
        --action destroy \
        --id '\${ {{$.Param "terraform/map-instance-id"}} }' \
        --name '\${ {{$.Param "terraform/map-instance-name"}} }'
EO_SYNC_DESTROY
    when    = destroy
  }
{{    else if eq ( $.Param "cloud/join-method" ) "discovery" -}}
# no create method defined, use the DRP Endpoints default discovery process

  provisioner "local-exec" {
    command = <<EO_DISC_CREATE
      ./synchronize.sh \
        --action create \
        --id '\${ {{$.Param "terraform/map-instance-id"}} }' \
        --name '\${ {{$.Param "terraform/map-instance-name"}} }' \
        --macaddr '\${ {{$.Param "terraform/map-mac-address"}} }' \
        --workflow '\${var.drp_{{$k}}_workflow}' \
        --pipeline '\${var.drp_{{$k}}_pipeline}' \
        --tags '\${var.drp_{{$k}}_tags}' \
        --icon '\${var.drp_{{$k}}_icon}' \
        --color '\${var.drp_{{$k}}_color}'
EO_DISC_CREATE
  }

  provisioner "local-exec" {
    command = <<EO_DISC_DESTROY
    ./synchronize.sh \
      --action destroy \
      --id '\${ {{$.Param "terraform/map-instance-id"}} }' \
      --name '\${ {{$.Param "terraform/map-instance-name"}} }'
EO_DISC_DESTROY
    when    = destroy
  }
{{    else -}}
# in 'cloud-provision-machines.tf.tmpl' template
# Param 'cloud/join-method' had no supported value, no join/unjoin operations specified
{{    end -}}
}
{{  end -}}
{{end -}}
