#!/usr/bin/env bash
pre_flash_loop_check() {
  drpcli machines remove $RS_UUID param dell-pending-flash-jobs &> /dev/null || :
  pj="$(awk -f pendingJobs.awk < <(/opt/dell/srvadmin/sbin/racadm jobqueue view) | sort)"
  if [[ $pj ]]; then
    if [[ $pj == '{{ .Param "dell-pending-flash-jobs" }}' ]]; then
      echo "Pending server config jobs did not resolve over a reboot.  Manual intervention required."
      echo "$pj"
      exit 1
    fi
    drpcli machines set $RS_UUID param dell-pending-flash-jobs to "$pj" &>/dev/null
    echo "Job queue has pending, running, or scheduled server config jobs - reboot to see if they clear"
    echo "$pj"
    exit_incomplete_reboot
  fi
}

flash_dell_bin() {
  FORCE=''
  [[ $WANT_FORCE == true ]] && FORCE="-f"
  chmod +x $FILENAME
  set -o pipefail
  set +e
  ./$FILENAME -q ${FORCE} |& tee $FILENAME.log
  case $? in
    0)
       echo "$FILENAME succeeded";;
    1)
       # If the system is up to date, this is the return code and
       # the log will have the message "No Applicable Updates Available"
       failed=yes
       fgrep -q "No Applicable Updates Available" $FILENAME.log && failed=''
       ;;
    2)
       echo "$FILENAME wants a reboot"
       want_reboot=yes;;
    3)
       echo "Soft error for $FILENAME"
       echo "Likely at the exact level already.";;
    4)
       echo "Hard error for $FILENAME"
       failed=yes;;
    5)
       echo "Unquailifed update for $FILENAME"
       echo "Either missing or not applicable - skipping";;
    6)
       echo "Reboot started!! for $FILENAME"
       want_reboot=yes;;
    9)
       echo "Package verify failed for $FILENAME"
       failed=yes;;
    13)
       echo "Dependency failure, but success!";;
    14)
       echo "Dependency failure, but reboot wanted"
       want_reboot=yes;;
    esac
    set +o pipefail
    set -e
}

flash_dell_exe() {
  [[ $IPMI_USER && $IPMI_PASS ]] || xiterr "Missing IPMI credentials"
  if [[ $WANT_FORCE = true ]]; then
    echo "Force flash not supported with Redfish based flashing for $FILENAME"
    echo "Please use .bin based flash packages if you need to downgrade firmware"
  fi
  echo "We should do better than this"
  yum -y install python3-requests || :
  echo "Setting up local link"
  racadm=/opt/dell/srvadmin/bin/idracadm7
  $racadm set idrac.os-bmc.AdminState Enabled || :
  COUNT=0
  while !  ip link | grep -q idrac ; do
    echo "no idrac - yet"
    sleep 30
    if (($COUNT > 10)) ; then
      echo "Failed to get idrac interface"
      exit 1
    fi
    COUNT=$((COUNT+1))
  done
  ip link set dev idrac up || :
  ip a a 169.254.1.2/24 dev idrac || :
  chmod +x ./dell-redfish.py
  mv $FILENAME $FILENAME.EXE
  set -o pipefail
  set +e
  ./dell-redfish.py --image=$FILENAME.EXE --location=. -ip 169.254.1.1 -u "$IPMI_USER" -p "$IPMI_PASS" |& tee $FILENAME.log
  case $? in
    0)
      echo "$FILENAME succeeded";;
    1)
      # If the system is up to date, this is the return code and
      # the log will have the message "No Applicable Updates Available"
      failed=yes
      fgrep -q "No Applicable Updates Available" $FILENAME.log && failed=''
      ;;
    2)
      echo "$FILENAME wants a reboot"
      want_reboot=yes;;
    3)
      echo "Soft error for $FILENAME"
      echo "Likely at the exact level already.";;
    4)
      echo "Hard error for $FILENAME"
      failed=yes;;
    5)
      echo "Unquailifed update for $FILENAME"
      echo "Either missing or not applicable - skipping";;
    6)
      echo "Reboot started!! for $FILENAME"
      want_reboot=yes;;
    9)
      echo "RPM verify failed for $FILENAME"
      failed=yes;;
    13)
      echo "Dependency failure, but success!";;
    14)
      echo "Dependency failure, but reboot wanted"
      want_reboot=yes;;
  esac
  set -e
  set +o pipefail

  echo "Disable idrac nic"
  $racadm set idrac.os-bmc.AdminState Disabled || :
}

flash_dell_filename() {
  case $FILETYPE in
    bin) flash_dell_bin;;
    exe) flash_dell_exe;;
    *) xiterr "Dell cannot flash unknown file type '$FILETYPE'";;
  esac
}