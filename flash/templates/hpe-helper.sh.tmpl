#!/usr/bin/env bash

{{ template "hpe-ilorest-helper.sh.tmpl" . }}

pre_flash_loop_check() {
  :
}

flash_hpe_rpm() {
  # Check to see if RPM is already installed.
  set +e
  RPMNAME=$(basename $FILENAME | sed "s/.rpm//")
  if ! rpm -q ${RPMNAME} 2>/dev/null >/dev/null ; then
    rpm -ivh $FILENAME
  fi
  # Find SETUP command
  SETUP=$(rpm -ql $RPMNAME | grep '/setup$')
  if [[ "$SETUP" = "" ]] ; then
    SETUP=$(rpm -ql $RPMNAME | grep '/hpsetup$')
  fi
  if [[ "$SETUP" = "" ]] ; then
    echo "Failed to find a setup script!"
    echo "Files in $RPMNAME"
    rpm -ql $RPMNAME
    exit 1
  fi
  set -e
  # One day make this an argument
  TPMARG=""
  if [[ "$RPMNAME" =~ ^firmware-system ]] ; then
    TPMARG="--tpmbypass"
  fi
  if [[ "$RPMNAME" =~ ^firmware-ilo ]] ; then
    TPMARG="--tpmbypass"
  fi
  cd $(dirname $SETUP)
  set -o pipefail
  FORCE=''
  FORCE_G=''
  if [[ $WANT_FORCE = true ]]; then
    FORCE='-f'
    FORCE_G='-g'
  fi
  if [[ "$RPMNAME" =~ ^firmware-nic-mellanox ]] ; then
    set +e
    $SETUP -s ${FORCE} |& tee $FILENAME.log
    case $? in
      0)
        if grep -iq "or reboot machine" $FILENAME.log ; then
          echo "Installed successfully - reboot required"
          want_reboot=yes
        else
          echo "Installed successfully - no reboot required"
        fi
        ;;
      1)
        echo "Nothing to do";;
      *)
        echo "$SETUP returned $?"
        failed=yes;;
    esac
    set -e
  else
    set +e
    $SETUP -s ${FORCE_G} ${TPMARG} ${FORCE} |& tee $FILENAME.log
    case $? in
      0)
        echo "Installed successfully - no reboot required";;
      1)
        echo "Installed successfully - reboot required"
        want_reboot=yes;;
      2)
        echo "Nothing to do - at current level";;
      3)
        echo "Nothing to do - current level or no matching hardware";;
      4)
        echo "Nothing to do - matching hardware doesn't have firmware";;
      5)
        echo "Nothing to do - user interrupted the install";;
      6)
        echo "Nothing to do - hardware disabled";;
      7)
        echo "A failure has occurred."
        failed=yes;;
      *)
        echo "$SETUP returned $?"
        failed=yes;;
    esac
    set -e
  fi
  set +o pipefail
  cd -
}

declare -a hpe_success_fw_list
{{ range $idx, $name := $.ParamExpand "flash/hpe-success-reboot-list" }}
hpe_success_fw_list[{{$idx}}]='{{$name}}'
{{ end }}

reboot_on_hpe_success() {
  for ((i=0;i<${#hpe_success_fw_list[@]};i++)); do
    [[ $1 =~ ${hpe_success_fw_list[$i]} ]] &&  want_reboot=true
  done
}

flash_hpe_fwpkg() {
  echo "Getting current inventory data"
  set +e
  safe_ilorest list Name Version Oem/Hpe/DeviceClass Oem/Hpe/DeviceContext Oem/Hpe/Targets --selector=SoftwareInventory --json > i_tmp.json
  RC=$?
  case $RC in
    23)
      echo "ILO reports 23?!?!?! This requires reboot to address."
      exit_incomplete_reboot
      ;;
    0)
      echo "inventory retrieved"
      ;;
    *)
      echo "Inventory failed: $RC"
      exit 1
      ;;
  esac
  # ILO Rest can add lines to the json that aren't correct
  grep -v "^Discover" i_tmp.json > inventory.json
  set -e

  echo "Getting id/version lists from fwpkg"
  rm -rf tmpdir
  mkdir tmpdir
  cd tmpdir
  unzip ../${FILENAME}
  jq '[{"DC": .DeviceClass, "Pieces": (.Devices.Device[]|{"Target": .Target, "Version": .Version, "Name": .DeviceName, "Reset": .FirmwareImages[].ResetRequired})}]' payload.json > ../comp.json
  cd ..

  echo "Set the present and install flags"
  present=false
  install=false
  why="unknown"

  while read line
  do
    DC=$(echo $line | awk -F'|' '{ print $1 }')
    TARGET=$(echo $line | awk -F'|' '{ print $2 }')
    VERSION=$(echo $line | awk -F'|' '{ print $3 }')
    VERSION=$(echo $VERSION|sed 's/^U[0-9]*[ \t]*//')
    RESET=$(echo $line | awk -F'|' '{ print $4 }')

    echo -n "Processing: $DC $TARGET $VERSION $RESET "
    count=$(jq ".|map(select((((.Oem.Hpe.DeviceClass|tostring)==\"$DC\") or ((.Oem.Hpe.DeviceClass|tostring)==\"null\")) and ([]+.Oem.Hpe.Targets|contains([\"$TARGET\"]))))|length" inventory.json)

    if [[ $count != 0 ]] ; then
      present=true
      CURVERSION=$(jq ".|map(select((((.Oem.Hpe.DeviceClass|tostring)==\"$DC\") or ((.Oem.Hpe.DeviceClass|tostring)==\"null\")) and ([]+.Oem.Hpe.Targets|contains([\"$TARGET\"]))))[0].Version" -r inventory.json)
      CURVERSION=$(echo $CURVERSION|sed 's/^U[0-9]*[ \t]*//')
      CURVERSION=$(echo $CURVERSION|sed 's/^A[0-9]*[ \t]*//')

      MYIFS=$IFS
      IFS='-._ ' read -ra new_ver <<< "$VERSION"
      IFS='-._ ' read -ra old_ver <<< "$CURVERSION"
      IFS=$MYIFS

      NL=${#new_ver[*]}
      OL=${#old_ver[*]}
      if (( $NL > $OL )) ; then
        CL=$OL
      else
        CL=$NL
      fi
      install=false
      why="Same value"
      for ((i=0; i<$CL; i++)) ; do
        bnv=${new_ver[$i]}
        if [[ $bnv =~ ^v ]] ; then
          bnv="${bnv:1}"
        fi
        bov=${old_ver[$i]}
        if [[ $bov =~ ^v ]] ; then
          bov="${bov:1}"
        fi
        nv=$(printf "%d" ${bnv} 2>/dev/null || echo "not valid")
        ov=$(printf "%d" ${bov} 2>/dev/null || echo "not valid")

        if [[ "$nv" == "0not valid" || "$ov" == "0not valid" ]] ; then
          if [[ "$bnv" != "$bov" ]] ; then
            install=false
            why="Not the same platform"
            break
          fi
          continue
        fi
        if (( nv == ov )); then
          install=false
          why="same value"
          continue
        fi
        if [[ $WANT_FORCE = true ]]; then
          install=true
          why="forced"
        elif (( nv < ov )); then
          install=false
          why="Current version newer than package"
        else
          install=true
          why="Package newer than current version"
        fi
        break
      done
    fi

    if [[ $install = true ]]; then
      break
    fi
  done <<< $(jq -r '.[]|.DC + "|" + .Pieces.Target + "|" + .Pieces.Version + "|" + (.Pieces.Reset|tostring)' comp.json)

  if [[ $present = false ]]; then
    echo "No component present for $FILENAME."
    return
  fi
  if [[ $install = false ]]; then
    echo "Not installing $FILENAME: $why"
  else
    rm -rf inst.fwpkg
    cp $FILENAME inst.fwpkg
    echo "Updating $FILENAME: $why"
    set -o pipefail
    set +e
    safe_ilorest flashfwpkg inst.fwpkg --tpmover |& tee flash.out
    case $? in
      0)
        if grep -iq "upload failed" flash.out ; then
          echo "Component is missing??? - skipping"
        else
          if grep -iq "reboot is required" flash.out ; then
            want_reboot=yes
          fi
          # Loop over parameter that means success needs reboot.
          reboot_on_hpe_success "$FILENAME"
        fi
        ;;
      *)
        failed=yes
        echo "Failed to flash with $FILENAME"
        ;;
    esac
    set +o pipefail
    set -e
  fi
}

flash_hpe_filename() {
  case $FILETYPE in
    rpm) flash_hpe_rpm;;
    fwpkg) flash_hpe_fwpkg;;
    *) xiterr "Cannot flash pkg type $FILETYPE for HPE";;
  esac
}