# CoreOS Content Bundle

## Overview

The CoreOS content bundle provides infrastructure for deploying and managing CoreOS-based systems, particularly Red Hat CoreOS (RHCOS) for OpenShift deployments. It delivers essential components for properly installing and configuring CoreOS in both live and persistent environments.

CoreOS serves as the foundation for OpenShift nodes, providing an immutable infrastructure approach with:
- Declarative configuration via Ignition
- Atomic updates
- Container-optimized design
- Enhanced security features

## Key Components

### Workflow Chain

The CoreOS installation process follows a comprehensive workflow chain:

```mermaid
flowchart LR
    %% Main horizontal flow with aligned nodes
    subgraph keySteps [Key Workflow Steps]
        direction LR
        discover[universal-discover] --> hardware[universal-hardware] --> burnin[universal-burnin] --> coreos-install[universal-coreos-install] --> pre-runbook[universal-pre-runbook] --> runbook[universal-runbook]
    end
    
    %% Final step outside of key workflow
    runbook --> complete[universal-complete]
    
    %% Additional entry points on top
    rebuild[universal-rebuild] --> discover
    maintenance[universal-maintenance] --> discover
    
    %% Entry points moved below
    evaluate[universal-evaluate] --> hardware
    start[universal-start] --> pre-runbook
```

### Profiles

- **`universal-application-coreos-base`**: The fundamental pipeline for CoreOS deployments

### Bootenvs

- **coreos-4-install**: Installation environment for CoreOS 4.x, supporting both x86_64 and aarch64 architectures

### Stages

The content bundle includes multiple stages that coordinate the installation process:
- **coreos-config**: Configures CoreOS 4.x settings
- **universal-coreos-install-classification-base**: Base classification for installation process
- **universal-coreos-install-\***: Various stages handling different phases of the installation

### Parameters

Key parameters include:
- **`coreos/in-os-ignition-template`**: Template for in-OS ignition configuration
- **`coreos/install-debug`**: Flag for enabling installation debugging
- **`coreos/json-config`**: JSON configuration for customizing CoreOS setup
- **`coreos/ignition-template`**: Template for ignition configuration
- **`coreos/ignition-url`**: URL for ignition template references

### Templates

The bundle provides several critical templates:
- **basic-ign.tmpl**: Base ignition configuration
- **basic-in-os-ign.tmpl**: In-OS ignition configuration
- **coreos.drpcli-\*.service.\*.tmpl**: systemd service definitions for DRP integration
- **embedded-in-os-ign.json.tmpl**: Template for embedding custom configurations
- **select-ign.tmpl** and **select-in-os-ign.tmpl**: Templates for ignition configuration selection

## Usage

### Installation Prerequisites

1. DRP environment with proper network configuration
2. Red Hat CoreOS (RHCOS) ISO available either locally or via URL
3. Required content bundles:
   - drp-community-content
   - OpenShift content bundle (when used for OpenShift deployments)

### Deployment Process

The CoreOS content bundle integrates with DRP's workflow system, particularly through:

1. **Discovery Phase**: Machine is discovered and initial hardware assessment is performed
2. **Hardware Configuration**: BIOS, firmware, and other hardware components are configured
3. **Burn-in Testing**: Optional hardware validation and stress testing
4. **CoreOS Installation**: The actual installation of CoreOS using ignition
5. **Pre-runbook Configuration**: Setup specific to the target environment
6. **Runbook Execution**: Final configuration and customization

### Customization

To customize CoreOS deployments:

1. **Ignition Configuration**: Modify by creating custom ignition templates and setting the `coreos/ignition-template` parameter
2. **JSON Configuration**: Add custom JSON via the `coreos/json-config` parameter
3. **Installation Options**: Configure installation devices via the `operating-system-disk` parameter
4. **SSH Access**: Manage SSH keys through the `access-keys` parameter
5. **Debugging**: Enable installation debugging via the `coreos/install-debug` parameter

### Integration with OpenShift

When used for OpenShift deployments, this content bundle provides the foundation for these OpenShift nodes:
- Bootstrap nodes
- Control plane nodes
- Worker nodes

Note that the load balancer nodes in OpenShift deployments currently use Photon OS rather than CoreOS.

The OpenShift content bundle builds upon this foundation to create a complete OpenShift cluster deployment solution.

## Troubleshooting

### Common Issues

1. **PXE Boot Failures**: Verify network connectivity and DHCP configuration
2. **Ignition Errors**: Check ignition configuration syntax and availability
3. **Installation Hangs**: Enable `coreos/install-debug` to prevent automatic reboots for investigation

### Debugging Techniques

1. Set `coreos/install-debug: true` to prevent automatic reboots after installation
2. Check DRP logs for installation progress and errors
3. Examine the ignition configuration via the machine's endpoint at `/machines/{uuid}/config.json`

## Support and Resources

- [Digital Rebar Documentation](https://docs.rackn.io)
- [Red Hat CoreOS Documentation](https://docs.openshift.com/container-platform/4.15/architecture/architecture-rhcos.html)
- [Ignition Configuration Specification](https://coreos.github.io/ignition/configuration-v3_5/)
